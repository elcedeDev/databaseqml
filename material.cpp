#include "material.h"
#include "database.h"

Material::Material(Database *parent) :
    QObject((QDeclarativeItem*)parent),
    m_Database(parent)
{

    m_iMaxTools = 40;
    m_iMaxLasTools = 20;
    m_iMaxMilTools = 20;

    m_id = 0;
    for(int i = 0; i <= m_iMaxLasTools; ++i)
    {
        MaterialTool* mt = new MaterialTool();
        mt->setlas_toolset(m_las_toolset);
        mt->setnum(i);
        m_tools.append(mt);
    }
    for(int i = m_iMaxLasTools + 1; i <= m_iMaxMilTools; ++i)
    {
        MilMaterialTool* mt = new MilMaterialTool();
        mt->setmil_toolset(m_mil_toolset);
        mt->setnum(i);
        m_tools.append(mt);
    }
}

Material::~Material()
{
    m_tools.clear();
}

QString Material::name() const
{
    return m_strFile;
}

void Material::setName(const QString &str)
{
    m_strFile = str;
}

QString Material::strFile() const
{
    return m_strFile;
}

void Material::setstrFile(const QString& str)
{
    m_strFile = str;
}

bool Material::bStripperboard() const
{
    return m_bStripperboard;
}

void Material::setbStripperboard(bool b)
{
    m_bStripperboard = b;
}

bool Material::bContourLookAhead() const
{
    return m_bContourLookAhead;
}

void Material::setbContourLookAhead(bool b)
{
    m_bContourLookAhead = b;
}

bool Material::DeleteTools_bActive() const
{
    return m_DeleteTools_bActive;
}

void Material::setDeleteTools_bActive(bool b)
{
    m_DeleteTools_bActive = b;
}

bool Material::OptimizeEmptyMoves_bActive() const
{
    return m_OptimizeEmptyMoves_bActive;
}

void Material::setOptimizeEmptyMoves_bActive(bool b)
{
    m_OptimizeEmptyMoves_bActive = b;
}

bool Material::bSteel() const
{
    return m_bSteel;
}

void Material::setbSteel(bool b)
{
    m_bSteel = b;
}

double Material::bDynamicControl() const
{
    return m_bDynamicControl;
}

void Material::setbDynamicControl(double d)
{
    m_bDynamicControl = d;
}

double Material::bDynamicFrequency() const
{
    return m_bDynamicFrequency;
}

void Material::setbDynamicFrequency(double d)
{
    m_bDynamicFrequency = d;
}

double Material::bDynamicPulseWidth() const
{
    return m_bDynamicPulseWidth;
}

void Material::setbDynamicPulseWidth(double d)
{
    m_bDynamicPulseWidth = d;
}

double Material::bDynamicAnalogPower() const
{
    return m_bDynamicAnalogPower;
}

void Material::setbDynamicAnalogPower(double d)
{
    m_bDynamicAnalogPower = d;
}

double Material::dThickness() const
{
    return m_dThickness;
}

void Material::setdThickness(double d)
{
    m_dThickness = d;
}

double Material::dSlopeTime() const
{
    return m_dSlopeTime;
}

void Material::setdSlopeTime(double d)
{
    m_dSlopeTime = d;
}

double Material::dCentripetalForceLimitFactor() const
{
    return m_dCentripetalForceLimitFactor;
}

void Material::setdCentripetalForceLimitFactor(double d)
{
    m_dCentripetalForceLimitFactor = d;
}

double Material::dMachineSpeedMax() const
{
    return m_dMachineSpeedMax;
}

void Material::setdMachineSpeedMax(double d)
{
    m_dMachineSpeedMax = d;
}

double Material::dMachineAccelerationMax() const
{
    return m_dMachineAccelerationMax;
}

void Material::setdMachineAccelerationMax(double d)
{
    m_dMachineAccelerationMax = d;
}

double Material::dBackgroundPower() const
{
    return m_dBackgroundPower;
}

void Material::setdBackgroundPower(double d)
{
    m_dBackgroundPower = d;
}

double Material::dRotaryDiameter() const
{
    return m_dRotaryDiameter;
}

void Material::setdRotaryDiameter(double d)
{
    m_dRotaryDiameter = d;
}

double Material::dCylinderRotaryDiameter() const
{
    return m_dCylinderRotaryDiameter;
}

void Material::setdCylinderRotaryDiameter(double d)
{
    m_dCylinderRotaryDiameter = d;
}

double Material::dZ1Height() const
{
    return m_dZ1Height;
}

void Material::setdZ1Height(double d)
{
    m_dZ1Height = d;
}

QString Material::strEngraverIniFile() const
{
    return m_strEngraverIniFile;
}

void Material::setstrEngraverIniFile(const QString &str)
{
    m_strEngraverIniFile = str;
}

QString Material::OptimizeEmptyMoves_strToolSequence() const
{
    return m_OptimizeEmptyMoves_strToolSequence;
}

void Material::setOptimizeEmptyMoves_strToolSequence(const QString &str)
{
    m_OptimizeEmptyMoves_strToolSequence = str;
}

QString Material::strSlaveMaterialFileName_0_20() const
{
    return m_strSlaveMaterialFileName_0_20;
}

void Material::setstrSlaveMaterialFileName_0_20(const QString &str)
{
    m_strSlaveMaterialFileName_0_20 = str;
}

QString Material::strSlaveMaterialFileName_21_40() const
{
    return m_strSlaveMaterialFileName_21_40;
}

void Material::setstrSlaveMaterialFileName_21_40(const QString &str)
{
    m_strSlaveMaterialFileName_21_40 = str;
}

int Material::iMaterial() const
{
    return m_iMaterial;
}

void Material::setiMaterial(const int &iVal)
{
    m_iMaterial = iVal;
}

int Material::iType() const
{
    return m_iType;
}

void Material::setiType(const int &iVal)
{
    m_iType = iVal;
}

double Material::dMarkSpeed() const
{
    return m_dMarkSpeed;
}

void Material::setdMarkSpeed(const double &dVal)
{
    m_dMarkSpeed = dVal;
}

int Material::las_toolset() const
{
    return m_las_toolset;
}

void Material::setlas_toolset(const int &iVal)
{
    m_las_toolset = iVal;
}

int Material::mil_toolset() const
{
    return m_mil_toolset;
}

void Material::setmil_toolset(const int &iVal)
{
    m_mil_toolset = iVal;
}


QList<QObject *> Material::tools() const
{
    return m_tools;
}

void Material::fetchTools()
{
    m_tools.clear();
    for(int i = 0; i <= m_iMaxLasTools; ++i)
    {
        MaterialTool* mt = new MaterialTool();
        mt->setlas_toolset(m_las_toolset);
        mt->setnum(i);
        mt->fetch(m_db);
        m_tools.append(mt);
    }
    for(int i = m_iMaxLasTools + 1; i <= m_iMaxMilTools; ++i)
    {
        MilMaterialTool* mt = new MaterialTool();
        mt->setlas_toolset(m_las_toolset);
        mt->setnum(i);
        mt->fetch(m_db);
        m_tools.append(mt);
    }
}

void Material::update()
{
    m_db->transaction();
    QSqlQuery q(*m_db);
        QString strSQL = QString("SELECT update_las_material(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

        q.prepare(strSQL);
        int i = 0;
        q.bindValue(i, m_strFile); ++i;
        q.bindValue(i, m_bStripperboard ); ++i;
        q.bindValue(i, m_bContourLookAhead); ++i;
        q.bindValue(i, m_DeleteTools_bActive); ++i;
        q.bindValue(i, m_OptimizeEmptyMoves_bActive); ++i;
        q.bindValue(i, m_bSteel); ++i;
        q.bindValue(i, m_bDynamicControl); ++i;
        q.bindValue(i, m_bDynamicFrequency); ++i;
        q.bindValue(i, m_bDynamicPulseWidth); ++i;
        q.bindValue(i, m_bDynamicAnalogPower); ++i;
        q.bindValue(i, m_dThickness); ++i;
        q.bindValue(i, m_dSlopeTime); ++i;
        q.bindValue(i, m_dCentripetalForceLimitFactor); ++i;
        q.bindValue(i, m_dMachineSpeedMax); ++i;
        q.bindValue(i, m_dMachineAccelerationMax); ++i;
        q.bindValue(i, m_dBackgroundPower); ++i;
        q.bindValue(i, m_dRotaryDiameter); ++i;
        q.bindValue(i, m_dCylinderRotaryDiameter); ++i;
        q.bindValue(i, m_dZ1Height); ++i;
        q.bindValue(i, m_strEngraverIniFile); ++i;
        q.bindValue(i, m_OptimizeEmptyMoves_strToolSequence); ++i;
        q.bindValue(i, m_strSlaveMaterialFileName_0_20); ++i;
        q.bindValue(i, m_strSlaveMaterialFileName_21_40); ++i;
        q.bindValue(i, m_iMaterial); ++i;
        q.bindValue(i, m_iType); ++i;
        q.bindValue(i, m_dMarkSpeed); ++i;
        q.bindValue(i, m_las_toolset); ++i;
        q.bindValue(i, m_mil_toolset);

        q.exec();

        if(q.first())
        {
            m_id = q.value(0).toInt();

            int j = 0;
            for(auto i = m_tools.begin(); i != m_tools.end(); ++i, ++j)
            {

                if( j <= m_las_toolset)
                {
                    ((MaterialTool*)(*i))->setlas_toolset(m_las_toolset);
                    if(((MaterialTool*)(*i))->update(m_db) == -1)
                        qDebug() << "ERROR -- Material::update() - tool->update - mat_id:" << m_id << "las_toolset:" << m_las_toolset  << "tool num:" << j;
                }
                else
                {
                    ((MaterialTool*)(*i))->setlas_toolset(m_mil_toolset);
                    if(((MilMaterialTool*)(*i))->update(m_db) == -1)
                        qDebug() << "ERROR -- Material::update() - tool->update - mat_id:" << m_id << "las_toolset:" << m_mil_toolset << "tool num:" << j;

                }
            }

        }
        else
        {
            QString str;
            QTextStream out(&str);
            out << "ERROR -- Material::update() ----- " << q.lastError().text();
            //qDebug() << "ERROR -- Material::update() ----- " << q.lastError();
            qDebug() << str;
            m_Database->setLasterror(str);
            m_db->rollback();
        }

        m_db->commit();
}

bool Material::fetch()
{

    QSqlQuery q(*m_db);
    QString strSQL = QString("SELECT * FROM las_material WHERE \"strFile\" = '%1';").arg(m_strFile);

    q.prepare(strSQL);

    q.exec();

    int i = 0;

    if(q.first())
    {
        m_id                                   = q.value(i).toInt();  ++i;
        m_strFile                              = q.value(i).toString();  ++i;
        m_bStripperboard                       = q.value(i).toBool();  ++i;
        m_bContourLookAhead                    = q.value(i).toBool();  ++i;
        m_DeleteTools_bActive                  = q.value(i).toBool();  ++i;
        m_OptimizeEmptyMoves_bActive           = q.value(i).toBool();  ++i;
        m_bSteel                               = q.value(i).toBool();  ++i;
        m_bDynamicControl                      = q.value(i).toBool();  ++i;
        m_bDynamicFrequency                    = q.value(i).toBool();  ++i;
        m_bDynamicPulseWidth                   = q.value(i).toBool();  ++i;
        m_bDynamicAnalogPower                  = q.value(i).toBool();  ++i;
        m_dThickness                           = q.value(i).toDouble();  ++i;
        m_dSlopeTime                           = q.value(i).toDouble();  ++i;
        m_dCentripetalForceLimitFactor         = q.value(i).toDouble();  ++i;
        m_dMachineSpeedMax                     = q.value(i).toDouble();  ++i;
        m_dMachineAccelerationMax              = q.value(i).toDouble();  ++i;
        m_dBackgroundPower                     = q.value(i).toDouble();  ++i;
        m_dRotaryDiameter                      = q.value(i).toDouble();  ++i;
        m_dCylinderRotaryDiameter              = q.value(i).toDouble();  ++i;
        m_dZ1Height                            = q.value(i).toDouble();  ++i;
        m_strEngraverIniFile                   = q.value(i).toString();  ++i;
        m_OptimizeEmptyMoves_strToolSequence   = q.value(i).toString();  ++i;
        m_strSlaveMaterialFileName_0_20        = q.value(i).toString();  ++i;
        m_strSlaveMaterialFileName_21_40       = q.value(i).toString(); ++i;
        m_iMaterial                            = q.value(i).toInt();  ++i;
        m_iType                                = q.value(i).toInt(); ++i;
        m_dMarkSpeed                           = q.value(i).toDouble(); ++i;
        m_las_toolset                          = q.value(i).toInt(); ++i;
        m_mil_toolset                          = q.value(i).toInt();

        fetchTools();
        return true;

    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR fetching material: " << m_strFile << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        //qDebug() << "ERROR fetching material: " << m_strFile << q.lastError();
        return false;
    }
}

int Material::copy(const QString strName)
{
    QSqlQuery q(*m_db);
    QString strSQL = QString("SELECT copy_las_material(?);");
    q.prepare(strSQL);
    q.bindValue(0, strName);

    q.exec();

    if(q.first())
    {
        //qDebug() << "SUCCESS: --- int Material::copy(const QString strName)" << strName;
        return q.value(0).toInt();
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        //qDebug() << "ERROR: cannot be executed: " << strSQL << q.lastError();
        return 0;
    }

}

bool Material::rename(const QString strOld, const QString strNew)
{
    if(! isValidName(strNew)) return false;

    QSqlQuery q(*m_db);
    QString strSQL = QString(" UPDATE las_material SET \"strFile\"= :newname WHERE \"strFile\"= :oldname returning id;");
    q.prepare(strSQL);
    q.bindValue(0, strNew);
    q.bindValue(1, strOld);

    q.exec();

    if(q.first())
        return true;
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << "  <0:>" << strNew << "  <1:>" << strOld << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        return false;
    }

}

bool Material::deleteMat(const QString strName)
{
    QSqlQuery q(*m_db);
    QString strSQL = QString("DELETE FROM las_material WHERE \"strFile\"= :strMat returning id;");
    q.prepare(strSQL);
    q.bindValue(0, strName);

    q.exec();

    if(q.first())
    {
        m_Database->setLasterror("");
        //qDebug() << "Material::deleteMat -- DELETED: " << strName;
        return true;
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text() << strName;
        qDebug() << str;
        m_Database->setLasterror(str);
        return false;
    }
}

int Material::newMat(const QString strName)
{
    m_db->transaction();
    QSqlQuery q(*m_db);
    QString strSQL = QString("INSERT INTO las_material(\"strFile\") VALUES(:strMat) returning id;");
    q.prepare(strSQL);
    q.bindValue(0, strName);

    q.exec();

    if(q.first())
    {
        /*
        m_id    = q.value(0).toInt();

        QSqlQuery q2(*m_db);
        strSQL = QString("INSERT INTO las_material_tool(mat_id, num) VALUES(:matid, :numval) returning num;");
        q2.prepare(strSQL);

        for(int i = 0; i < 41; ++i)
        {
            q2.bindValue(0, m_id);
            q2.bindValue(1, i);

            if(!q2.exec()) {
                QString str;
                QTextStream out(&str);
                out  << "ERROR: cannot be executed: " << strSQL << q2.lastError().text();
                qDebug() << str;
                m_Database->setLasterror(str);
                m_db->rollback();
                return -i;
            }
        }
        */
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        m_db->rollback();
        return 0;
    }
    m_db->commit();
    return m_id;

}

bool Material::isValidName(const QString strName)
{
    if(!strName.length()) return false;

    QSqlQuery q(*m_db);
    QString strSQL = QString("SELECT count(*) FROM las_material WHERE \"strFile\"= :strMat;");
    q.prepare(strSQL);
    q.bindValue(0, strName);
    q.exec();

    if(q.first())
    {
        //qDebug() << "bool Material::isValidName(const QString strName)" << strName << "q.value(0).toBool()" << q.value(0).toBool() << "q.value(0).toInt()" << q.value(0).toInt();
        return !(q.value(0).toBool());
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);

        return false;
    }
}

void Material::setDatabase(QSqlDatabase *db)
{
    m_db = db;
}

