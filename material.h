#ifndef MATERIAL_H
#define MATERIAL_H

#include <QObject>
#include <QDeclarativeItem>
#include <QtSql>
#include "materialtool.h" //laser tools
#include "mil_materialtool.h"


QT_FORWARD_DECLARE_CLASS(Database)

class Material : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name                              READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString strFile                           READ strFile WRITE setstrFile NOTIFY strFileChanged)
    Q_PROPERTY(bool    bStripperboard                    READ bStripperboard WRITE setbStripperboard NOTIFY bStripperboardChanged)
    Q_PROPERTY(bool    bContourLookAhead                 READ bContourLookAhead WRITE setbContourLookAhead NOTIFY bContourLookAheadChanged)
    Q_PROPERTY(bool    DeleteTools_bActive               READ DeleteTools_bActive WRITE setDeleteTools_bActive NOTIFY DeleteTools_bActiveChanged)
    Q_PROPERTY(bool    OptimizeEmptyMoves_bActive        READ OptimizeEmptyMoves_bActive WRITE setOptimizeEmptyMoves_bActive NOTIFY OptimizeEmptyMoves_bActiveChanged)
    Q_PROPERTY(bool    bSteel                            READ bSteel WRITE setbSteel NOTIFY bSteelChanged)
    Q_PROPERTY(double  bDynamicControl                   READ bDynamicControl WRITE setbDynamicControl NOTIFY bDynamicControlChanged)
    Q_PROPERTY(double  bDynamicFrequency                 READ bDynamicFrequency WRITE setbDynamicFrequency NOTIFY bDynamicFrequencyChanged)
    Q_PROPERTY(double  bDynamicPulseWidth                READ bDynamicPulseWidth WRITE setbDynamicPulseWidth NOTIFY bDynamicPulseWidthChanged)
    Q_PROPERTY(double  bDynamicAnalogPower               READ bDynamicAnalogPower WRITE setbDynamicAnalogPower NOTIFY bDynamicAnalogPowerChanged)
    Q_PROPERTY(double  dThickness                        READ dThickness WRITE setdThickness NOTIFY dThicknessChanged)
    Q_PROPERTY(double  dSlopeTime                        READ dSlopeTime WRITE setdSlopeTime NOTIFY dSlopeTimeChanged)
    Q_PROPERTY(double  dCentripetalForceLimitFactor      READ dCentripetalForceLimitFactor WRITE setdCentripetalForceLimitFactor NOTIFY dCentripetalForceLimitFactorChanged)
    Q_PROPERTY(double  dMachineSpeedMax                  READ dMachineSpeedMax WRITE setdMachineSpeedMax NOTIFY dMachineSpeedMaxChanged)
    Q_PROPERTY(double  dMachineAccelerationMax           READ dMachineAccelerationMax WRITE setdMachineAccelerationMax NOTIFY dMachineAccelerationMaxChanged)
    Q_PROPERTY(double  dBackgroundPower                  READ dBackgroundPower WRITE setdBackgroundPower NOTIFY dBackgroundPowerChanged)
    Q_PROPERTY(double  dRotaryDiameter                   READ dRotaryDiameter WRITE setdRotaryDiameter NOTIFY dRotaryDiameterChanged)
    Q_PROPERTY(double  dCylinderRotaryDiameter           READ dCylinderRotaryDiameter WRITE setdCylinderRotaryDiameter NOTIFY dCylinderRotaryDiameterChanged)
    Q_PROPERTY(double  dZ1Height                         READ dZ1Height WRITE setdZ1Height NOTIFY dZ1HeightChanged)
    Q_PROPERTY(QString strEngraverIniFile                READ strEngraverIniFile WRITE setstrEngraverIniFile NOTIFY strEngraverIniFileChanged)
    Q_PROPERTY(QString OptimizeEmptyMoves_strToolSequence READ OptimizeEmptyMoves_strToolSequence WRITE setOptimizeEmptyMoves_strToolSequence NOTIFY OptimizeEmptyMoves_strToolSequenceChanged)
    Q_PROPERTY(QString strSlaveMaterialFileName_0_20     READ strSlaveMaterialFileName_0_20 WRITE setstrSlaveMaterialFileName_0_20 NOTIFY strSlaveMaterialFileName_0_20Changed)
    Q_PROPERTY(QString strSlaveMaterialFileName_21_40    READ strSlaveMaterialFileName_21_40 WRITE setstrSlaveMaterialFileName_21_40 NOTIFY strSlaveMaterialFileName_21_40Changed)
    Q_PROPERTY(int     iMaterial                         READ iMaterial WRITE setiMaterial NOTIFY iMaterialChanged)
    Q_PROPERTY(int     iType                             READ iType WRITE setiType NOTIFY iTypeChanged)
    Q_PROPERTY(double  dMarkSpeed                        READ dMarkSpeed WRITE setdMarkSpeed NOTIFY dMarkSpeedChanged)
    Q_PROPERTY(int     las_toolset                       READ las_toolset WRITE setlas_toolset NOTIFY las_toolsetChanged)
    Q_PROPERTY(int     mil_toolset                       READ mil_toolset WRITE setmil_toolset NOTIFY mil_toolsetChanged)

    Q_PROPERTY(QList<QObject*> tools                     READ tools NOTIFY toolsChanged)



public:
    explicit Material(Database *parent = 0);
    ~Material();

//copy constructor
//    Material(const Material &m);
//properties

    QString name() const;
    Q_INVOKABLE void setName(const QString&);


    QString strFile() const;
    Q_INVOKABLE void setstrFile(const QString&);

    bool bStripperboard() const;
    Q_INVOKABLE void setbStripperboard(bool);

    bool bContourLookAhead() const;
    Q_INVOKABLE void setbContourLookAhead(bool);

    bool DeleteTools_bActive() const;
    Q_INVOKABLE void setDeleteTools_bActive(bool);

    bool OptimizeEmptyMoves_bActive() const;
    Q_INVOKABLE void setOptimizeEmptyMoves_bActive(bool);

    bool bSteel() const;
    Q_INVOKABLE void setbSteel(bool);

    double bDynamicControl() const;
    Q_INVOKABLE void setbDynamicControl(double);

    double bDynamicFrequency() const;
    Q_INVOKABLE void setbDynamicFrequency(double);

    double bDynamicPulseWidth() const;
    Q_INVOKABLE void setbDynamicPulseWidth(double);

    double bDynamicAnalogPower() const;
    Q_INVOKABLE void setbDynamicAnalogPower(double);

    double dThickness() const;
    Q_INVOKABLE void setdThickness(double);

    double dSlopeTime() const;
    Q_INVOKABLE void setdSlopeTime(double);

    double dCentripetalForceLimitFactor() const;
    Q_INVOKABLE void setdCentripetalForceLimitFactor(double);

    double dMachineSpeedMax() const;
    Q_INVOKABLE void setdMachineSpeedMax(double);

    double dMachineAccelerationMax() const;
    Q_INVOKABLE void setdMachineAccelerationMax(double);

    double dBackgroundPower() const;
    Q_INVOKABLE void setdBackgroundPower(double);

    double dRotaryDiameter() const;
    Q_INVOKABLE void setdRotaryDiameter(double);

    double dCylinderRotaryDiameter() const;
    Q_INVOKABLE void setdCylinderRotaryDiameter(double);

    double dZ1Height() const;
    Q_INVOKABLE void setdZ1Height(double);

    QString strEngraverIniFile() const;
    Q_INVOKABLE void setstrEngraverIniFile(const QString&);

    QString OptimizeEmptyMoves_strToolSequence() const;
    Q_INVOKABLE void setOptimizeEmptyMoves_strToolSequence(const QString&);

    QString strSlaveMaterialFileName_0_20() const;
    Q_INVOKABLE void setstrSlaveMaterialFileName_0_20(const QString&);

    QString strSlaveMaterialFileName_21_40() const;
    Q_INVOKABLE void setstrSlaveMaterialFileName_21_40(const QString&);

    int iMaterial() const;
    Q_INVOKABLE void setiMaterial(const int&);

    int iType() const;
    Q_INVOKABLE void setiType(const int&);

    double dMarkSpeed() const;
    Q_INVOKABLE void setdMarkSpeed(const double&);

    int las_toolset() const;
    Q_INVOKABLE void setlas_toolset(const int&);

    int mil_toolset() const;
    Q_INVOKABLE void setmil_toolset(const int&);

    QList<QObject*> tools() const;
    //Q_INVOKABLE void settools(const QList<QObject*>&);

    void fetchTools();
    Q_INVOKABLE void update();
    Q_INVOKABLE bool fetch();
    Q_INVOKABLE int  copy(const QString);
    Q_INVOKABLE bool rename(const QString, const QString);
    Q_INVOKABLE bool deleteMat(const QString);
    Q_INVOKABLE int  newMat(const QString);
    Q_INVOKABLE bool isValidName(const QString);

    void setDatabase(QSqlDatabase*);


signals:
    void nameChanged();
    void strFileChanged();
    void bStripperboardChanged();
    void bContourLookAheadChanged();
    void DeleteTools_bActiveChanged();
    void OptimizeEmptyMoves_bActiveChanged();
    void bSteelChanged();
    void bDynamicControlChanged();
    void bDynamicFrequencyChanged();
    void bDynamicPulseWidthChanged();
    void bDynamicAnalogPowerChanged();
    void dThicknessChanged();
    void dSlopeTimeChanged();
    void dCentripetalForceLimitFactorChanged();
    void dMachineSpeedMaxChanged();
    void dMachineAccelerationMaxChanged();
    void dBackgroundPowerChanged();
    void dRotaryDiameterChanged();
    void dCylinderRotaryDiameterChanged();
    void dZ1HeightChanged();
    void strEngraverIniFileChanged();
    void OptimizeEmptyMoves_strToolSequenceChanged();
    void strSlaveMaterialFileName_0_20Changed();
    void strSlaveMaterialFileName_21_40Changed();
    void iMaterialChanged();
    void iTypeChanged();
    void dMarkSpeedChanged();
    void las_toolsetChanged();
    void mil_toolsetChanged();
    void toolsChanged();

public slots:


private:

    //QString     m_name;
    QSqlDatabase*   m_db;
    int             m_id;
    QString         m_strFile;
    bool            m_bStripperboard;
    bool            m_bContourLookAhead;
    bool            m_DeleteTools_bActive;
    bool            m_OptimizeEmptyMoves_bActive;
    bool            m_bSteel;
    bool            m_bDynamicControl;
    bool            m_bDynamicFrequency;
    bool            m_bDynamicPulseWidth;
    bool            m_bDynamicAnalogPower;
    double          m_dThickness;
    double          m_dSlopeTime;
    double          m_dCentripetalForceLimitFactor;
    double          m_dMachineSpeedMax;
    double          m_dMachineAccelerationMax;
    double          m_dBackgroundPower;
    double          m_dRotaryDiameter;
    double          m_dCylinderRotaryDiameter;
    double          m_dZ1Height;
    QString         m_strEngraverIniFile;
    QString         m_OptimizeEmptyMoves_strToolSequence;
    QString         m_strSlaveMaterialFileName_0_20;
    QString         m_strSlaveMaterialFileName_21_40;
    int             m_iMaterial;
    int             m_iType;
    double          m_dMarkSpeed;
    int             m_las_toolset;
    int             m_mil_toolset;

    int             m_iMaxTools;
    int             m_iMaxLasTools;
    int             m_iMaxMilTools;
    Database*       m_Database;

    QList<QObject*> m_tools;

};

#endif // MATERIAL_H
