#ifndef MATERIALTOOLSET_H
#define MATERIALTOOLSET_H

#include <QObject>

class MaterialToolset : public QObject
{
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int mac_type READ mac_type WRITE setMac_type NOTIFY mac_typeChanged)

    Q_OBJECT
public:
    explicit MaterialToolset(QObject *parent = 0);
    ~MaterialToolset();

    int id() const;
    Q_INVOKABLE void setId(const int);

    QString name() const;
    Q_INVOKABLE void setName(const QString &name);

    int mac_type() const;
    Q_INVOKABLE void setMac_Type(const int);


signals:
    void idChanged();
    void nameChanged();
    void mac_typeChanged();

public slots:

private:
    int         m_id;
    QString     m_name;
    int         m_mac_type;

};

#endif // MATERIALTOOLSET_H
