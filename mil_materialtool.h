#ifndef MILMATERIALTOOL_H
#define MILMATERIALTOOL_H

#include <QObject>
#include <QtSql>

class MilMaterialTool : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int      mil_toolset READ mil_toolset WRITE setmil_toolset NOTIFY mil_toolsetChanged)
    Q_PROPERTY(int      num READ num WRITE setnum NOTIFY numChanged)
    Q_PROPERTY(QString  strNote READ strNote WRITE setstrNote NOTIFY strNoteChanged)
    Q_PROPERTY(int      iType READ iType WRITE setiType NOTIFY iTypeChanged)
    Q_PROPERTY(double   dDiameter READ dDiameter WRITE setdDiameter NOTIFY  dDiameterChanged)
    Q_PROPERTY(double   dChannelWidth READ dChannelWidth WRITE setdChannelWidth NOTIFY dChannelWidthChanged)
    Q_PROPERTY(double   dSpindleSpeed READ dSpindleSpeed WRITE setdSpindleSpeed NOTIFY dSpindleSpeedChanged)
    Q_PROPERTY(double   dSpeedMax READ dSpeedMax WRITE setdSpeedMax NOTIFY dSpeedMaxChanged)
    Q_PROPERTY(double   dAccelerationMax READ dAccelerationMax WRITE setdAccelerationMax NOTIFY dAccelerationMaxChanged)
    Q_PROPERTY(double   dWorkHeight READ dWorkHeight WRITE setdWorkHeight NOTIFY dWorkHeightChanged)
    Q_PROPERTY(double   dDistanceDepth READ dDistanceDepth WRITE setdDistanceDepth NOTIFY  dDistanceDepthChanged)
    Q_PROPERTY(double   dPiercingSpeed READ dPiercingSpeed WRITE setdPiercingSpeed NOTIFY dPiercingSpeedChanged)
    Q_PROPERTY(double   dToolTest READ dToolTest WRITE setdToolTest NOTIFY dToolTestChanged)
    Q_PROPERTY(double   dAngle READ dAngle WRITE setdAngle NOTIFY dAngleChanged)
    Q_PROPERTY(int      iPocketType READ iPocketType WRITE setiPocketType NOTIFY iPocketTypeChanged)
    Q_PROPERTY(int      iPocketTool READ iPocketTool WRITE setiPocketTool NOTIFY iPocketToolChanged)
    Q_PROPERTY(bool     bSmoothenEdges READ bSmoothenEdges WRITE setbSmoothenEdges NOTIFY bSmoothenEdgesChanged)
    Q_PROPERTY(bool     bShortenEdges READ bShortenEdges WRITE setbShortenEdges NOTIFY bShortenEdgesChanged)
    Q_PROPERTY(bool     bTurnClockwise READ bTurnClockwise WRITE setbTurnClockwise NOTIFY bTurnClockwiseChanged)
    Q_PROPERTY(bool     bLubrication READ bLubrication WRITE setbLubrication NOTIFY bLubricationChanged)
    Q_PROPERTY(bool     bBoarderOnly READ bBoarderOnly WRITE setbBoarderOnly NOTIFY bbBoarderOnlyChanged)
    Q_PROPERTY(bool     bDelete READ bDelete WRITE setbDelete NOTIFY bDeleteChanged)
    Q_PROPERTY(bool     ToolTest_bActive READ ToolTest_bActive WRITE setToolTest_bActive NOTIFY  ToolTest_bActiveChanged)
    Q_PROPERTY(bool     bZMillCompensation READ bZMillCompensation WRITE setbZMillCompensation NOTIFY bZMillCompensationChanged)
    Q_PROPERTY(int      iEdges READ iEdges WRITE setiEdges NOTIFY iEdgesChanged)
    Q_PROPERTY(double   dStepover READ dStepover WRITE setdStepover NOTIFY dStepoverChanged)
    Q_PROPERTY(int      iFunctionType READ iFunctionType WRITE setiFunctionType NOTIFY iFunctionTypeChanged)

public:
    explicit MilMaterialTool(QObject *parent = 0);

    int mil_toolset() const;
    Q_INVOKABLE void setmil_toolset(const int);

    int num() const;
    Q_INVOKABLE void setnum(const int);

    QString strNote() const;
    Q_INVOKABLE void setstrNote(const QString&);

    int     iType() const;
    Q_INVOKABLE void setiType(const int);

    double dDiameter() const;
    Q_INVOKABLE void setdDiameter(const double);

    double dChannelWidth() const;
    Q_INVOKABLE void setdChannelWidth(const double);

    double dSpindleSpeed() const;
    Q_INVOKABLE void setdSpindleSpeed(const double);

    double dSpeedMax() const;
    Q_INVOKABLE void setdSpeedMax(const double);

    double dAccelerationMax() const;
    Q_INVOKABLE void setdAccelerationMax(const double);

    double dWorkHeight() const;
    Q_INVOKABLE void setdWorkHeight(const double);

    double dDistanceDepth() const;
    Q_INVOKABLE void setdDistanceDepth(const double);

    double dPiercingSpeed() const;
    Q_INVOKABLE void setdPiercingSpeed(const double);

    double dToolTest() const;
    Q_INVOKABLE void setdToolTest(const double);

    double dAngle() const;
    Q_INVOKABLE void setdAngle(const double);

    int     iPocketTool() const;
    Q_INVOKABLE void setiPocketTool(const int);

    int     iPocketType() const;
    Q_INVOKABLE void setiPocketType(const int);

    bool bSmoothenEdges() const;
    Q_INVOKABLE void setbSmoothenEdges(const bool);

    bool bShortenEdges() const;
    Q_INVOKABLE void setbShortenEdges(const bool);

    bool bTurnClockwise() const;
    Q_INVOKABLE void setbTurnClockwise(const bool);

    bool bLubrication() const;
    Q_INVOKABLE void setbLubrication(const bool);

    bool bBoarderOnly() const;
    Q_INVOKABLE void setbBoarderOnly(const bool);

    bool bDelete() const;
    Q_INVOKABLE void setbDelete(const bool);

    bool    ToolTest_bActive() const;
    Q_INVOKABLE void setToolTest_bActive(const bool);

    bool bZMillCompensation() const;
    Q_INVOKABLE void setbZMillCompensation(const bool);

    int     iEdges() const;
    Q_INVOKABLE void setiEdges(const int);

    double dStepover() const;
    Q_INVOKABLE void setdStepover(const double);

    int     iFunctionType() const;
    Q_INVOKABLE void setiFunctionType(const int);


    bool fetch(QSqlDatabase*);
    int update(QSqlDatabase*);


signals:
    void mil_toolsetChanged();
    void numChanged();
    void strNoteChanged();
    void iTypeChanged();
    void dDiameterChanged();
    void dChannelWidthChanged();
    void dSpindleSpeedChanged();
    void dSpeedMaxChanged();
    void dAccelerationMaxChanged();
    void dWorkHeightChanged();
    void dDistanceDepthChanged();
    void dPiercingSpeedChanged();
    void bTurnClockwiseChanged();
    void bShortenEdgesChanged();
    void bSmoothenEdgesChanged();
    void bLubricationChanged();
    void bDeleteChanged();
    void ToolTest_bActiveChanged();
    void dToolTestChanged();
    void dAngleChanged();
    void iPocketToolChanged();
    void iPocketTypeChanged();
    void bZMillCompensationChanged();
    void bbBoarderOnlyChanged();
    void iEdgesChanged();
    void dStepoverChanged();
    void iFunctionTypeChanged();



public slots:

private:
    int     m_mat_id;
    int     m_num;
    QString m_strNote;
    int     m_iType;
    double  m_dDiameter;
    double  m_dChannelWidth;
    double  m_dSpindleSpeed;
    double  m_dSpeedMax;
    double  m_dAccelerationMax;
    double  m_dWorkHeight;
    double  m_dDistanceDepth;
    double  m_dPiercingSpeed;
    double  m_dToolTest;
    double  m_dAngle;
    int     m_iPocketTool;
    int     m_iPocketType;
    bool    m_bSmoothenEdges;
    bool    m_bShortenEdges;
    bool    m_bTurnClockwise;
    bool    m_bLubrication;
    bool    m_bBoarderOnly;
    bool    m_bDelete;
    bool    m_ToolTest_bActive;
    bool    m_bZMillCompensation;
    int     m_iEdges;
    double  m_dStepover;
    int     m_iFunctionType;

};

#endif // MILMATERIALTOOL_H
