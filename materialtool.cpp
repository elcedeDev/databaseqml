#include "materialtool.h"
#include <QtSql>
#include <QStringBuilder>
#include <QMap>


MaterialTool::MaterialTool(QObject *parent) :
    QObject(parent)
{
    m_strNote = "Tool Note";
}

int MaterialTool::las_toolset() const
{
    return m_las_toolset;
}

void MaterialTool::setlas_toolset(const int i)
{
    m_las_toolset = i;
}

int MaterialTool::num() const
{
 return m_num;
}

void MaterialTool::setnum(const int i)
{
    m_num = i;
}

bool MaterialTool::bBoarderOnly() const
{
    return m_bBoarderOnly;
}

void MaterialTool::setbBoarderOnly(const bool b)
{
    m_bBoarderOnly = b;
}

bool MaterialTool::bDelete() const
{
    return m_bDelete;
}

void MaterialTool::setbDelete(const bool b)
{
    m_bDelete = b;
}

bool MaterialTool::bEngravingTool() const
{
    return m_bEngravingTool;
}

void MaterialTool::setbEngravingTool(const bool b)
{
    m_bEngravingTool = b;
}

bool MaterialTool::bSlotShortEdges() const
{
    return m_bSlotShortEdges;
}

void MaterialTool::setbSlotShortEdges(const bool b)
{
    m_bSlotShortEdges = b;
}

bool MaterialTool::bSlotSmoothenEdges() const
{
    return m_bSlotSmoothenEdges;
}

void MaterialTool::setbSlotSmoothenEdges(const bool b)
{
    m_bSlotSmoothenEdges = b;
}

bool MaterialTool::bSlotTurnClockwise() const
{
    return m_bSlotTurnClockwise;
}

void MaterialTool::setbSlotTurnClockwise(const bool b)
{
    m_bSlotTurnClockwise = b;
}

bool MaterialTool::bZMillCompensation() const
{
    return m_bZMillCompensation;
}

void MaterialTool::setbZMillCompensation(const bool b)
{
    m_bZMillCompensation = b;
}

double MaterialTool::dAccelerationMax() const
{
    return m_dAccelerationMax;
}

void MaterialTool::setdAccelerationMax(const double d)
{
    m_dAccelerationMax = d;
}

double MaterialTool::dAccLsrFrequency() const
{
    return m_dAccLsrFrequency;
}

void MaterialTool::setdAccLsrFrequency(const double d)
{
    m_dAccLsrFrequency = d;
}

double MaterialTool::dAccLsrPulseWidth() const
{
    return m_dAccLsrPulseWidth;
}

void MaterialTool::setdAccLsrPulseWidth(const double d)
{
    m_dAccLsrPulseWidth = d;
}

double MaterialTool::dAnalogOutput() const
{
    return m_dAnalogOutput;
}

void MaterialTool::setdAnalogOutput(const double d)
{
    m_dAnalogOutput = d;
}

double MaterialTool::dAngle() const
{
    return m_dAngle;
}

void MaterialTool::setdAngle(const double d)
{
    m_dAngle = d;
}

double MaterialTool::dBeamRotation() const
{
    return m_dBeamRotation;
}

void MaterialTool::setdBeamRotation(const double d)
{
    m_dBeamRotation = d;
}

double MaterialTool::dChannelWidth() const
{
    return m_dChannelWidth;
}

void MaterialTool::setdChannelWidth(const double d)
{
    m_dChannelWidth = d;
}

double MaterialTool::dConstLsrFrequency() const
{
    return m_dConstLsrFrequency;
}

void MaterialTool::setdConstLsrFrequency(const double d)
{
    m_dConstLsrFrequency = d;
}

double MaterialTool::dConstLsrPulseWidth() const
{
    return m_dConstLsrPulseWidth;
}

void MaterialTool::setdConstLsrPulseWidth(const double d)
{
    m_dConstLsrPulseWidth = d;
}

double MaterialTool::dDeccLsrFrequency() const
{
    return m_dDeccLsrFrequency;
}

void MaterialTool::setdDeccLsrFrequency(const double d)
{
    m_dDeccLsrFrequency = d;
}


double MaterialTool::dDeccLsrPulseWidth() const
{
    return m_dDeccLsrPulseWidth;
}

void MaterialTool::setdDeccLsrPulseWidth(const double d)
{
    m_dDeccLsrPulseWidth = d;
}

double MaterialTool::dDiameter() const
{
    return m_dDiameter;
}

void MaterialTool::setdDiameter(const double d)
{
    m_dDiameter = d;
}

double MaterialTool::dDistanceDepth() const
{
    return m_dDistanceDepth;
}

void MaterialTool::setdDistanceDepth(const double d)
{
    m_dDistanceDepth = d;
}

double MaterialTool::dEnvelopeCut() const
{
    return m_dEnvelopeCut;
}

void MaterialTool::setdEnvelopeCut(const double d)
{
    m_dEnvelopeCut = d;
}

double MaterialTool::dHeightXm() const
{
    return m_dHeightXm;
}

void MaterialTool::setdHeightXm(const double d)
{
    m_dHeightXm = d;
}

double MaterialTool::dHeightXmYm() const
{
    return m_dHeightXmYm;
}

void MaterialTool::setdHeightXmYm(const double d)
{
    m_dHeightXmYm = d;
}

double MaterialTool::dHeightXmYp() const
{
    return m_dHeightXmYp;
}

void MaterialTool::setdHeightXmYp(const double d)
{
    m_dHeightXmYp = d;
}

double MaterialTool::dHeightXp() const
{
    return m_dHeightXp;
}

void MaterialTool::setdHeightXp(const double d)
{
    m_dHeightXp = d;
}

double MaterialTool::dHeightXpYm() const
{
    return m_dHeightXpYm;
}

void MaterialTool::setdHeightXpYm(const double d)
{
    m_dHeightXpYm = d;
}

double MaterialTool::dHeightXpYp() const
{
    return m_dHeightXpYp;
}

void MaterialTool::setdHeightXpYp(const double d)
{
    m_dHeightXpYp = d;
}

double MaterialTool::dHeightYm() const
{
    return m_dHeightYm;
}

void MaterialTool::setdHeightYm(const double d)
{
    m_dHeightYm = d;
}

double MaterialTool::dHeightYp() const
{
    return m_dHeightYp;
}

void MaterialTool::setdHeightYp(const double d)
{
    m_dHeightYp = d;
}

double MaterialTool::dLookAheadLimitAngleMin() const
{
    return m_dLookAheadLimitAngleMin;
}

void MaterialTool::setdLookAheadLimitAngleMin(const double d)
{
    m_dLookAheadLimitAngleMin = d;
}


double MaterialTool::dLookAheadLinitAngleMax() const
{
    return m_dLookAheadLinitAngleMax;
}

void MaterialTool::setdLookAheadLinitAngleMax(const double d)
{
    m_dLookAheadLinitAngleMax = d;
}


double MaterialTool::dLsrAnalogPower() const
{
    return m_dLsrAnalogPower;
}

void MaterialTool::setdLsrAnalogPower(const double d)
{
    m_dLsrAnalogPower = d;
}

double MaterialTool::dLsrExponentAnalogPower() const
{
    return m_dLsrExponentAnalogPower;
}

void MaterialTool::setdLsrExponentAnalogPower(const double d)
{
    m_dLsrExponentAnalogPower = d;
}

double MaterialTool::dLsrExponentFrequency() const
{
    return m_dLsrExponentFrequency;
}

void MaterialTool::setdLsrExponentFrequency(const double d)
{
    m_dLsrExponentFrequency = d;
}

double MaterialTool::dLsrFactorAnalogPower() const
{
    return m_dLsrFactorAnalogPower;
}

void MaterialTool::setdLsrFactorAnalogPower(const double d)
{
    m_dLsrFactorAnalogPower = d;
}

double MaterialTool::dLsrFactorFrequency() const
{
    return m_dLsrFactorFrequency;
}

void MaterialTool::setdLsrFactorFrequency(const double d)
{
    m_dLsrFactorFrequency = d;
}

double MaterialTool::dMaxDeviation() const
{
    return m_dMaxDeviation;
}

void MaterialTool::setdMaxDeviation(const double d)
{
    m_dMaxDeviation = d;
}


double MaterialTool::dMaxPulseWidthSpeed() const
{
    return m_dMaxPulseWidthSpeed;
}

void MaterialTool::setdMaxPulseWidthSpeed(const double d)
{
    m_dMaxPulseWidthSpeed = d;
}

double MaterialTool::dPiercingSpeed() const
{
    return m_dPiercingSpeed;
}

void MaterialTool::setdPiercingSpeed(const double d)
{
    m_dPiercingSpeed = d;
}


double MaterialTool::dSpeedMax() const
{
    return m_dSpeedMax;
}

void MaterialTool::setdSpeedMax(const double d)
{
    m_dSpeedMax = d;
}


double MaterialTool::dSpindleSpeed() const
{
    return m_dSpindleSpeed;
}

void MaterialTool::setdSpindleSpeed(const double d)
{
    m_dSpindleSpeed = d;
}


double MaterialTool::dStartMacroAnalogPower() const
{
    return m_dStartMacroAnalogPower;
}

void MaterialTool::setdStartMacroAnalogPower(const double d)
{
    m_dStartMacroAnalogPower = d;
}


double MaterialTool::dStartMacroLsrFrequency() const
{
    return m_dStartMacroLsrFrequency;
}

void MaterialTool::setdStartMacroLsrFrequency(const double d)
{
    m_dStartMacroLsrFrequency = d;
}


double MaterialTool::dStartMacroLsrPulseWidth() const
{
    return m_dStartMacroLsrPulseWidth;
}

void MaterialTool::setdStartMacroLsrPulseWidth(const double d)
{
    m_dStartMacroLsrPulseWidth = d;
}


double MaterialTool::dStartMacroTime() const
{
    return m_dStartMacroTime;
}

void MaterialTool::setdStartMacroTime(const double d)
{
    m_dStartMacroTime = d;
}

double MaterialTool::dStopMacroTime() const
{
    return m_dStopMacroTime;
}

void MaterialTool::setdStopMacroTime(const double d)
{
    m_dStopMacroTime = d;
}


double MaterialTool::dStopMacroAnalogPower() const
{
    return m_dStopMacroAnalogPower;
}

void MaterialTool::setdStopMacroAnalogPower(const double d)
{
    m_dStopMacroAnalogPower = d;
}


double MaterialTool::dStopMacroLsrFrequency() const
{
    return m_dStopMacroLsrFrequency;
}

void MaterialTool::setdStopMacroLsrFrequency(const double d)
{
    m_dStopMacroLsrFrequency = d;
}


double MaterialTool::dStopMacroLsrPulseWidth() const
{
    return m_dStopMacroLsrPulseWidth;
}

void MaterialTool::setdStopMacroLsrPulseWidth(const double d)
{
    m_dStopMacroLsrPulseWidth = d;
}


double MaterialTool::dToolTest() const
{
    return m_dToolTest;
}

void MaterialTool::setdToolTest(const double d)
{
    m_dToolTest = d;
}


double MaterialTool::dWorkHeight() const
{
    return m_dWorkHeight;
}

void MaterialTool::setdWorkHeight(const double d)
{
    m_dWorkHeight = d;
}


double MaterialTool::dWorkSpeedXm() const
{
    return m_dWorkSpeedXm;
}

void MaterialTool::setdWorkSpeedXm(const double d)
{
    m_dWorkSpeedXm = d;
}


double MaterialTool::dWorkSpeedXmYm() const
{
    return m_dWorkSpeedXmYm;
}

void MaterialTool::setdWorkSpeedXmYm(const double d)
{
    m_dWorkSpeedXmYm = d;
}


double MaterialTool::dWorkSpeedXmYp() const
{
    return m_dWorkSpeedXmYp;
}

void MaterialTool::setdWorkSpeedXmYp(const double d)
{
    m_dWorkSpeedXmYp = d;
}

double MaterialTool::dWorkSpeedXp() const
{
    return m_dWorkSpeedXp;
}

void MaterialTool::setdWorkSpeedXp(const double d)
{
    m_dWorkSpeedXp = d;
}


double MaterialTool::dWorkSpeedXpYm() const
{
    return m_dWorkSpeedXpYm;
}

void MaterialTool::setdWorkSpeedXpYm(const double d)
{
    m_dWorkSpeedXpYm = d;
}


double MaterialTool::dWorkSpeedXpYp() const
{
    return m_dWorkSpeedXpYp;
}

void MaterialTool::setdWorkSpeedXpYp(const double d)
{
    m_dWorkSpeedXpYp = d;
}


double MaterialTool::dWorkSpeedYm() const
{
    return m_dWorkSpeedYm;
}

void MaterialTool::setdWorkSpeedYm(const double d)
{
    m_dWorkSpeedYm = d;
}



double MaterialTool::dWorkSpeedYp() const
{
    return m_dWorkSpeedYp;
}

void MaterialTool::setdWorkSpeedYp(const double d)
{
    m_dWorkSpeedYp = d;
}


int MaterialTool::iControlType() const
{
    return m_iControlType;
}

void MaterialTool::setiControlType(const int i)
{
    m_iControlType = i;
}


int MaterialTool::iPocketTool() const
{
    return m_iPocketTool;
}

void MaterialTool::setiPocketTool(const int i)
{
    m_iPocketTool = i;
}


int MaterialTool::iPocketType() const
{
    return m_iPocketType;
}

void MaterialTool::setiPocketType(const int i)
{
    m_iPocketType = i;
}

int MaterialTool::iType() const
{
    return m_iType;
}

void MaterialTool::setiType(const int i)
{
    m_iType = i;
}

QString MaterialTool::strNote() const
{
    return m_strNote;
}

void MaterialTool::setstrNote(const QString &str)
{
    m_strNote = str;
}

bool MaterialTool::ToolTest_bActive() const
{
    return m_ToolTest_bActive;
}

void MaterialTool::setToolTest_bActive(const bool b)
{
    m_ToolTest_bActive = b;
}

int MaterialTool::iToolType() const
{
    return m_iType;
}

void MaterialTool::setiToolType(const int i)
{
    m_iType = i;
}

bool MaterialTool::bPulseOn() const
{
    return m_bPulseOn;
}

void MaterialTool::setbPulseOn(const bool b)
{
    m_bPulseOn = b;
}

double MaterialTool::dLsrSpotDistance() const
{
    return m_dLsrSpotDistance;
}

void MaterialTool::setdLsrSpotDistance(const double d)
{
    m_dLsrSpotDistance = d;
}

bool MaterialTool::fetch(QSqlDatabase* db)
{
    QSqlQuery q(*db);
    QString strSQL;
    strSQL = QString("SELECT * FROM las_material_tool WHERE mat_id = %1 AND num = %2")
                .arg(m_las_toolset)
                .arg(m_num);
    q.exec(strSQL);
    if(q.first())
    {
        int i = 0;

        m_las_toolset                      = q.value(i).toInt(); ++i;
        m_num                         = q.value(i).toInt(); ++i;
        m_bBoarderOnly                = q.value(i).toBool(); ++i;
        m_bDelete                     = q.value(i).toBool(); ++i;
        m_bEngravingTool              = q.value(i).toBool(); ++i;
        m_bSlotShortEdges             = q.value(i).toBool(); ++i;
        m_bSlotSmoothenEdges          = q.value(i).toBool(); ++i;
        m_bSlotTurnClockwise          = q.value(i).toBool(); ++i;
        m_bZMillCompensation          = q.value(i).toBool(); ++i;
        m_dAccelerationMax            = q.value(i).toDouble(); ++i;
        m_dAccLsrFrequency            = q.value(i).toDouble(); ++i;
        m_dAccLsrPulseWidth           = q.value(i).toDouble(); ++i;
        m_dAnalogOutput               = q.value(i).toDouble(); ++i;
        m_dAngle                      = q.value(i).toDouble(); ++i;
        m_dBeamRotation               = q.value(i).toDouble(); ++i;
        m_dChannelWidth               = q.value(i).toDouble(); ++i;
        m_dConstLsrFrequency          = q.value(i).toDouble(); ++i;
        m_dConstLsrPulseWidth         = q.value(i).toDouble(); ++i;
        m_dDeccLsrFrequency           = q.value(i).toDouble(); ++i;
        m_dDeccLsrPulseWidth          = q.value(i).toDouble(); ++i;
        m_dDiameter                   = q.value(i).toDouble(); ++i;
        m_dDistanceDepth              = q.value(i).toDouble(); ++i;
        m_dEnvelopeCut                = q.value(i).toDouble(); ++i;
        m_dHeightXm                   = q.value(i).toDouble(); ++i;
        m_dHeightXmYm                 = q.value(i).toDouble(); ++i;
        m_dHeightXmYp                 = q.value(i).toDouble(); ++i;
        m_dHeightXp                   = q.value(i).toDouble(); ++i;
        m_dHeightXpYm                 = q.value(i).toDouble(); ++i;
        m_dHeightXpYp                 = q.value(i).toDouble(); ++i;
        m_dHeightYm                   = q.value(i).toDouble(); ++i;
        m_dHeightYp                   = q.value(i).toDouble(); ++i;
        m_dLookAheadLimitAngleMin     = q.value(i).toDouble(); ++i;
        m_dLookAheadLinitAngleMax     = q.value(i).toDouble(); ++i;
        m_dLsrAnalogPower             = q.value(i).toDouble(); ++i;
        m_dLsrExponentAnalogPower     = q.value(i).toDouble(); ++i;
        m_dLsrExponentFrequency       = q.value(i).toDouble(); ++i;
        m_dLsrFactorAnalogPower       = q.value(i).toDouble(); ++i;
        m_dLsrFactorFrequency         = q.value(i).toDouble(); ++i;
        m_dMaxDeviation               = q.value(i).toDouble(); ++i;
        m_dMaxPulseWidthSpeed         = q.value(i).toDouble(); ++i;
        m_dPiercingSpeed              = q.value(i).toDouble(); ++i;
        m_dSpeedMax                   = q.value(i).toDouble(); ++i;
        m_dSpindleSpeed               = q.value(i).toDouble(); ++i;
        m_dStartMacroAnalogPower      = q.value(i).toDouble(); ++i;
        m_dStartMacroLsrFrequency     = q.value(i).toDouble(); ++i;
        m_dStartMacroLsrPulseWidth    = q.value(i).toDouble(); ++i;
        m_dStartMacroTime             = q.value(i).toDouble(); ++i;
        m_dStopMacroTime              = q.value(i).toDouble(); ++i;
        m_dStopMacroAnalogPower       = q.value(i).toDouble(); ++i;
        m_dStopMacroLsrFrequency      = q.value(i).toDouble(); ++i;
        m_dStopMacroLsrPulseWidth     = q.value(i).toDouble(); ++i;
        m_dToolTest                   = q.value(i).toDouble(); ++i;
        m_dWorkHeight                 = q.value(i).toDouble(); ++i;
        m_dWorkSpeedXm                = q.value(i).toDouble(); ++i;
        m_dWorkSpeedXmYm              = q.value(i).toDouble(); ++i;
        m_dWorkSpeedXmYp              = q.value(i).toDouble(); ++i;
        m_dWorkSpeedXp                = q.value(i).toDouble(); ++i;
        m_dWorkSpeedXpYm              = q.value(i).toDouble(); ++i;
        m_dWorkSpeedXpYp              = q.value(i).toDouble(); ++i;
        m_dWorkSpeedYm                = q.value(i).toDouble(); ++i;
        m_dWorkSpeedYp                = q.value(i).toDouble(); ++i;
        m_iControlType                = q.value(i).toInt(); ++i;
        m_iPocketTool                 = q.value(i).toInt(); ++i;
        m_iPocketType                 = q.value(i).toInt(); ++i;
        m_iType                       = q.value(i).toInt(); ++i;
        m_strNote                     = q.value(i).toString(); ++i;
        m_ToolTest_bActive            = q.value(i).toBool(); ++i;
        m_iToolType                   = q.value(i).toInt(); ++i;
        m_bPulseOn                    = q.value(i).toBool();  ++i;
        m_dLsrSpotDistance            = q.value(i).toDouble();
        return true;
    }
    else
    {
        qDebug() << "ERROR -- MaterialTool::fetchTool -- tool not found! mat_id:" << m_las_toolset << "tool num:" << m_num << strSQL << q.lastError();
        return false;
    }

}

int MaterialTool::update(QSqlDatabase* db)
{
    QSqlQuery q(*db);
    QString strSQL = QString("SELECT update_las_material_tool(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

    q.prepare(strSQL);
    int i = 0;
    q.bindValue(i, m_las_toolset ); ++i;
    q.bindValue(i, m_num );     ++i;
    q.bindValue(i, m_bBoarderOnly ); ++i;
    q.bindValue(i, m_bDelete ); ++i;
    q.bindValue(i, m_bEngravingTool ); ++i;
    q.bindValue(i, m_bSlotShortEdges ); ++i;
    q.bindValue(i, m_bSlotSmoothenEdges ); ++i;
    q.bindValue(i, m_bSlotTurnClockwise ); ++i;
    q.bindValue(i, m_bZMillCompensation ); ++i;
    q.bindValue(i, m_dAccelerationMax ); ++i;
    q.bindValue(i, m_dAccLsrFrequency ); ++i;
    q.bindValue(i, m_dAccLsrPulseWidth ); ++i;
    q.bindValue(i, m_dAnalogOutput ); ++i;
    q.bindValue(i, m_dAngle ); ++i;
    q.bindValue(i, m_dBeamRotation ); ++i;
    q.bindValue(i, m_dChannelWidth ); ++i;
    q.bindValue(i, m_dConstLsrFrequency ); ++i;
    q.bindValue(i, m_dConstLsrPulseWidth ); ++i;
    q.bindValue(i, m_dDeccLsrFrequency ); ++i;
    q.bindValue(i, m_dDeccLsrPulseWidth ); ++i;
    q.bindValue(i, m_dDiameter ); ++i;
    q.bindValue(i, m_dDistanceDepth ); ++i;
    q.bindValue(i, m_dEnvelopeCut ); ++i;
    q.bindValue(i, m_dHeightXm ); ++i;
    q.bindValue(i, m_dHeightXmYm ); ++i;
    q.bindValue(i, m_dHeightXmYp ); ++i;
    q.bindValue(i, m_dHeightXp ); ++i;
    q.bindValue(i, m_dHeightXpYm ); ++i;
    q.bindValue(i, m_dHeightXpYp ); ++i;
    q.bindValue(i, m_dHeightYm ); ++i;
    q.bindValue(i, m_dHeightYp ); ++i;
    q.bindValue(i, m_dLookAheadLimitAngleMin ); ++i;
    q.bindValue(i, m_dLookAheadLinitAngleMax ); ++i;
    q.bindValue(i, m_dLsrAnalogPower ); ++i;
    q.bindValue(i, m_dLsrExponentAnalogPower ); ++i;
    q.bindValue(i, m_dLsrExponentFrequency ); ++i;
    q.bindValue(i, m_dLsrFactorAnalogPower ); ++i;
    q.bindValue(i, m_dLsrFactorFrequency ); ++i;
    q.bindValue(i, m_dMaxDeviation ); ++i;
    q.bindValue(i, m_dMaxPulseWidthSpeed ); ++i;
    q.bindValue(i, m_dPiercingSpeed ); ++i;
    q.bindValue(i, m_dSpeedMax ); ++i;
    q.bindValue(i, m_dSpindleSpeed ); ++i;
    q.bindValue(i, m_dStartMacroAnalogPower ); ++i;
    q.bindValue(i, m_dStartMacroLsrFrequency ); ++i;
    q.bindValue(i, m_dStartMacroLsrPulseWidth ); ++i;
    q.bindValue(i, m_dStartMacroTime ); ++i;
    q.bindValue(i, m_dStopMacroTime ); ++i;
    q.bindValue(i, m_dStopMacroAnalogPower ); ++i;
    q.bindValue(i, m_dStopMacroLsrFrequency ); ++i;
    q.bindValue(i, m_dStopMacroLsrPulseWidth ); ++i;
    q.bindValue(i, m_dToolTest ); ++i;
    q.bindValue(i, m_dWorkHeight ); ++i;
    q.bindValue(i, m_dWorkSpeedXm ); ++i;
    q.bindValue(i, m_dWorkSpeedXmYm ); ++i;
    q.bindValue(i, m_dWorkSpeedXmYp ); ++i;
    q.bindValue(i, m_dWorkSpeedXp ); ++i;
    q.bindValue(i, m_dWorkSpeedXpYm ); ++i;
    q.bindValue(i, m_dWorkSpeedXpYp ); ++i;
    q.bindValue(i, m_dWorkSpeedYm ); ++i;
    q.bindValue(i, m_dWorkSpeedYp ); ++i;
    q.bindValue(i, m_iControlType ); ++i;
    q.bindValue(i, m_iPocketTool ); ++i;
    q.bindValue(i, m_iPocketType ); ++i;
    q.bindValue(i, m_iType ); ++i;
    q.bindValue(i, m_strNote ); ++i;
    q.bindValue(i, m_ToolTest_bActive ); ++i;
    q.bindValue(i, m_iToolType ); ++i;
    q.bindValue(i, m_bPulseOn ); ++i;
    q.bindValue(i, m_dLsrSpotDistance);
    q.exec();

    //qDebug() << "m_mat_id" << m_mat_id << "m_num:" << m_num << " m_strNote:" << m_strNote ;

    //qDebug() << "fields: " << m_num << "," << m_bBoarderOnly<< ","    << m_bDelete<< ","     << m_bEngravingTool<< ","     << m_bSlotShortEdges<< ","     << m_bSlotSmoothenEdges<< ","     << m_bSlotTurnClockwise<< ","     << m_bZMillCompensation<< ","     << m_dAccelerationMax<< ","     << m_dAccLsrFrequency<< ","     << m_dAccLsrPulseWidth<< ","     << m_dAnalogOutput<< ","     << m_dAngle<< ","     << m_dBeamRotation<< ","     << m_dChannelWidth<< ","     << m_dConstLsrFrequency<< ","     << m_dConstLsrPulseWidth<< ","     << m_dDeccLsrFrequency<< ","     << m_dDeccLsrPulseWidth<< ","     << m_dDiameter<< ","     << m_dDistanceDepth<< ","     << m_dEnvelopeCut<< ","     << m_dHeightXm<< ","     << m_dHeightXmYm<< ","     << m_dHeightXmYp<< ","     << m_dHeightXp<< ","     << m_dHeightXpYm<< ","     << m_dHeightXpYp<< ","     << m_dHeightYm<< ","     << m_dHeightYp<< ","     << m_dLookAheadLimitAngleMin<< ","     << m_dLookAheadLinitAngleMax<< ","     << m_dLsrAnalogPower<< ","     << m_dLsrExponentAnalogPower<< ","     << m_dLsrExponentFrequency<< ","     << m_dLsrFactorAnalogPower<< ","     << m_dLsrFactorFrequency<< ","     << m_dMaxDeviation<< ","     << m_dMaxPulseWidthSpeed<< ","     << m_dPiercingSpeed<< ","     << m_dSpeedMax<< ","     << m_dSpindleSpeed<< ","     << m_dStartMacroAnalogPower<< ","     << m_dStartMacroLsrFrequency<< ","     << m_dStartMacroLsrPulseWidth<< ","     << m_dStartMacroTime<< ","     << m_dStopMacroAnalogPower<< ","     << m_dStopMacroLsrFrequency<< ","     << m_dStopMacroLsrPulseWidth<< ","     << m_dToolTest<< ","     << m_dWorkHeight<< ","     << m_dWorkSpeedXm<< ","     << m_dWorkSpeedXmYm<< ","     << m_dWorkSpeedXmYp<< ","     << m_dWorkSpeedXp<< ","     << m_dWorkSpeedXpYm<< ","     << m_dWorkSpeedXpYp<< ","     << m_dWorkSpeedYm<< ","     << m_dWorkSpeedYp<< ","     << m_iControlType<< ","     << m_iPocketTool<< ","     << m_iPocketType<< ","     << m_iType << ","    << m_strNote << ","    << m_ToolTest_bActive << ","    << m_iToolType  << ","   << m_bPulseOn<< ","     << m_dLsrSpotDistance;
/*
    QMap<QString,QVariant> boundMap = q.boundValues();
    for(auto i = boundMap.begin(); i != boundMap.end(); ++i)
    {
        qDebug() << "boundMap: " << i.key() << i.value();
    }
*/

    if(q.first())
    {
        if( m_num != q.value(0).toInt())
        {
            qDebug() << "1 ERROR running MaterialTool::update() -- num: " << m_num << "  " << q.lastError();
            return -1;
        }
    }
    else
    {
        qDebug() << "2 ERROR running MaterialTool::update() -- num: " << m_num << "  " << q.lastError();
        return -1;
    }
    return m_num;

}
