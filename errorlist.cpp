#include "errorlist.h"

ErrorListItem::ErrorListItem(QObject *parent) :
    QObject(parent)
{
}

ErrorListItem::~ErrorListItem()
{}

QString ErrorListItem::name() const
{
    return m_name;
}

void ErrorListItem::setName(const QString &name)
{
    m_name = name;
}

QString ErrorListItem::text() const
{
    return m_text;
}

void ErrorListItem::setText(const QString &text)
{
    m_text = text;
}

qint16 ErrorListItem::num() const
{
    return m_num;
}

void ErrorListItem::setNum(const qint16 &num)
{
    m_num = num;
}

