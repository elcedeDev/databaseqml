#include "database.h"
//#include <QtSql>
#include <QMessageBox>
#include <QDebug>
#include "version.h"


Database::Database(QDeclarativeItem *parent):
    QDeclarativeItem(parent)
{
    // need to disable this flag to draw inside a QDeclarativeItem
    setFlag(QGraphicsItem::ItemHasNoContents, false);

    m_driver = QString("QPSQL");

    m_strLanguage = "en";

    m_Material = new Material(this);
    m_Material->setDatabase(&m_db);

    m_milMaterial = new MilMaterial(this);
    m_milMaterial->setDatabase(&m_db);
    PRINTVERSION
/*
    QString str;
    str[0] = QChar(0x03A3);
    str[1] = QChar(0x20AC);
    str[3] = 'Ñ';
    str[4] = 'ß';
    str[5] = '€';
    //qDebug() <<  str;
    m_result = str;
*/
  //  QObject::connect(&*this, SIGNAL(dbLanguageChanged()),
  //  parent, SLOT(dbLanguageChanged()));
}

Database::~Database()
{
    m_errorList.clear();
    m_warningList.clear();
    m_lang.clear();
    m_db.close();
    if(m_Material) delete m_Material;
}

QString Database::dbname() const
{
    return m_dbname;
}


void Database::setDbName(const QString &dbname)
{
    m_dbname = dbname;
}

QString Database::username() const
{
    return m_username;
}

void Database::setUsername(const QString &username)
{
    m_username = username;
}

QString Database::password() const
{
    return m_password;
}

void Database::setPassword(const QString &password)
{
    m_password = password;
}

QString Database::hostname() const
{
    return m_hostname;
}

void Database::setHostname(const QString &hostname)
{
    m_hostname = hostname;
}

int Database::port() const
{
    return m_port;
}

void Database::setPort(const int &port)
{
    m_port = port;
}

QString Database::result() const
{
    return m_result;
}

void Database::setResult(const QString &result)
{
    m_result = result;
}

QList<QObject*> Database::errorList()
{
    return m_errorList;
}

QList<QObject*> Database::warningList()
{
    return m_warningList;
}



Material* Database::material() const
{
    return m_Material;
}

void Database::fetchMaterial(const QString &str)
{
    m_Material->setName(str);
    m_Material->fetch();

}

bool Database::fetchMaterial(const int &i)
{
    QString strSQL = QString("SELECT \"strFile\" FROM las_material WHERE id =%1").arg(i);
    QSqlQuery q(strSQL, m_db);
    QString strName;

    if(q.first())
    {
        strName = q.value(0).toString();
        m_Material->setName(strName);
        m_Material->fetch();
        return true;
    }
    return false;
}

QStringList Database::materials()
{
     QStringList list;
     QSqlQuery q("SELECT \"strFile\" FROM las_material ORDER BY  \"strFile\"", m_db);
     while(q.next())
     {
         list.append(q.value(0).toString());
     }
     return list;
}

MilMaterial *Database::milMaterial() const
{
   return m_milMaterial;
}

void Database::fetchMilMaterial(const QString &str)
{

    m_milMaterial->setName(str);
    m_milMaterial->fetch();
}

bool Database::fetchMilMaterial(const int &i)
{
    QString strSQL = QString("SELECT \"strFile\" FROM mil_material WHERE id =%1").arg(i);
    QSqlQuery q(strSQL, m_db);
    QString strName;

    if(q.first())
    {
        strName = q.value(0).toString();
        m_milMaterial->setName(strName);
        m_milMaterial->fetch();
        return true;
    }
    return false;
}

QStringList Database::milMaterials()
{
    QStringList list;
    QSqlQuery q("SELECT \"strFile\" FROM mil_material ORDER BY  \"strFile\"", m_db);
    while(q.next())
    {
        list.append(q.value(0).toString());
    }
    return list;
}

void Database::refreshErrorList()
{
    m_errorList.clear();
    m_warningList.clear();

    QString strLang = getLang().toLower();

    QString strSQL = QString("SELECT * FROM las_get_errors('%1')").arg(strLang);
    QSqlQuery query(strSQL, m_db);

    while (query.next()) {
        ErrorListItem* p = new ErrorListItem();
        p->setName(query.value(1).toString());
        p->setText(query.value(2).toString());
        p->setNum(query.value(0).toInt());
        m_errorList.append(p);
        //qDebug() << p->name() << " "  << p->text() << " " << p->num();
    }

    qDebug() << "QSqlQuery " << strSQL << " -- executed";

    strSQL = QString("SELECT * FROM las_get_warnings('%1')").arg(strLang);
    query.exec(strSQL);

    while (query.next()) {
        ErrorListItem* p = new ErrorListItem();
        p->setName(query.value(1).toString());
        p->setText(query.value(2).toString());
        p->setNum(query.value(0).toInt());
        m_warningList.append(p);
        //qDebug() << p->name() << " "  << p->text() << " " << p->num();
    }

    qDebug() << "QSqlQuery " << strSQL << " -- executed";
}

QString Database::tr()
{
    return "";
}



QString Database::lasterror() const
{
    return m_lasterror;
}


 void  Database::setLasterror(const QString &strError)
{
    m_lasterror = strError;
}


bool Database::connect()
{
    if(m_db.isOpen()) return true;

    //qDebug() << "Database plugin: -- Try to connect -- driver: " << m_driver << " db name:" << m_dbname;

    QSqlError err;
    m_db = QSqlDatabase::addDatabase(m_driver, m_dbname);
    m_db.setDatabaseName(m_dbname);
    m_db.setHostName(m_hostname);
    m_db.setPort(m_port);
    if (!m_db.open(m_username, m_password)) {
        err = m_db.lastError();
        //m_db = QSqlDatabase();

        //qDebug() << "Database plugin: -- An error occurred while opening the connection 1: " << err.text();

        QSqlDatabase::removeDatabase(m_dbname);
        if (err.type() != QSqlError::NoError){
            m_result =  QString("An error occurred while opening the connection: " + err.text());
            qDebug() << "Database plugin: -- An error occurred while opening the connection: " << err.text();
            return false;
        }
    }

    m_result = "Database connected";
    qDebug() << "Database plugin: -- Database connected ----------------------------- 1";


    //QString strSQL = "SELECT * FROM errors WHERE type = 0";

    QString strLang = getLang().toLower();

    QString strSQL = QString("SELECT * FROM las_get_errors('%1')").arg(strLang);
    QSqlQuery query(strSQL, m_db);

    while (query.next()) {
        ErrorListItem* p = new ErrorListItem();
        p->setName(query.value(1).toString());
        p->setText(query.value(2).toString());
        p->setNum(query.value(0).toInt());
        m_errorList.append(p);
        //qDebug() << p->name() << " "  << p->text() << " " << p->num();
    }

    qDebug() << "QSqlQuery " << strSQL << " -- executed";

    strSQL = QString("SELECT * FROM las_get_warnings('%1')").arg(strLang);
    query.exec(strSQL);

    while (query.next()) {
        ErrorListItem* p = new ErrorListItem();
        p->setName(query.value(1).toString());
        p->setText(query.value(2).toString());
        p->setNum(query.value(0).toInt());
        m_warningList.append(p);
        //qDebug() << p->name() << " "  << p->text() << " " << p->num();
    }

    qDebug() << "QSqlQuery " << strSQL << " -- executed";


    loadLanguage();

    qDebug() << "Database plugin: -- Database connected ----------------------------- 2";
    emit connected();

    return true;
}


QString Database::dbTr(QString str) const
{
    QStringList strArr = str.split(QRegExp("\\n"));
    QString strTr;

    if( strArr.length() > 1)
    {

        for(int i = 0; i < strArr.length(); ++i)
        {
            if(i != strArr.length() - 1)
                strTr += strArr[i]+ QString("\\n");
            else
                strTr += strArr[i];
        }
    }
    else
        strTr = str;

    QMap<QString, QString>::const_iterator i = m_lang.constFind(strTr);
    if(i != m_lang.end()){
        if(i.value().length())
        {
            QString strRes = i.value();
            strArr = strRes.split("\\n");
            if( strArr.length() > 1)
            {
                strTr = "";
                for(int j = 0; j < strArr.length(); ++j)
                {
                    if(j != strArr.length() - 1)
                        strTr += strArr[j]+'\n';
                    else
                        strTr += strArr[j];
                }
                return strTr;
            }
            return i.value();

        }else{
         return str;
        }
    }
    return str;
}




bool Database::execute(QString strSQL)
{
    QSqlQuery query;
    query.exec(strSQL);
    qDebug() << query.result();
    return true;
}

QString Database::language() const
{
    return m_strLanguage;
}

void Database::setLanguage(const QString &strLanguage)
{
    m_strLanguage = strLanguage;

    loadLanguage();
    refreshErrorList();
    //qDebug() << "-------- void Database::setLanguage(const QString &strLanguage) --  emit dbLanguageChanged();";
    emit dbLanguageChanged();
}

void Database::doTranslate()
{
    //qDebug() << "-------- void Database::doTranslate() ----------------------------  emit dbLanguageChanged();";
    //emit dbLanguageChanged();
    //this function mayne is not needed
    //TODO: remove this
}

QList<QObject *> Database::las_toolsetList()
{
    m_las_toolsetList.clear();

    QString strSQL = QString("SELECT * FROM las_toolset WHERE mac_type = ('%1')").arg(strLang);
    QSqlQuery query(strSQL, m_db);

    while (query.next()) {
        ErrorListItem* p = new ErrorListItem();
        p->setName(query.value(1).toString());
        p->setText(query.value(2).toString());
        p->setNum(query.value(0).toInt());
        m_errorList.append(p);
        //qDebug() << p->name() << " "  << p->text() << " " << p->num();
    }
}

QString Database::strMac_type() const
{
    return m_strMac_type;
}

void Database::setStrMac_type(const QString &str)
{

}

void Database::loadLanguage()
{
    m_lang.clear();
    QString strLang = getLang();

    QString strSQL;
    //strSQL = QString("SELECT \"ID\", \"%1\" FROM language").arg(strLang);
    strSQL = QString("SELECT \"ID\", COALESCE(\"%1\", \"ENG\", '') FROM language").arg(strLang);

    QSqlQuery query(strSQL, m_db);

    while (query.next()) {
        m_lang[query.value(0).toString()] = query.value(1).toByteArray();
        //qDebug() << query.value(1).toByteArray();
    }
}

QString Database::getLang()
{
    if(m_strLanguage == "en") {
       return "ENG";
    }
    else if(m_strLanguage == "de"){
       return "GER";
    }
    else if(m_strLanguage == "hu"){
       return "HUN";
    }
    else if(m_strLanguage == "jp"){
       return "JPN";
    }
    else if(m_strLanguage == "fr"){
       return "fre";
    }
    else if(m_strLanguage == "es"){
       return "ESP";
    }
    else if(m_strLanguage == "dk"){
       return "DNK";
    }
    else {
       return "ENG";
    }
}

QString getLastExecutedQuery(const QSqlQuery& query)
{
 QString str = query.lastQuery();
 QMapIterator<QString, QVariant> it(query.boundValues());
 int i = 1;
 while (it.hasNext())
 {
  it.next();
  //str.replace("?",it.value().toString());
  qDebug() << "val:" << i << it.key() << it.value().toString();
  ++i;
 }
 return str;
}

//![1]
/*
void Database::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
}
*/






