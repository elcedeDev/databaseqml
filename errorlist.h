#ifndef ERRORLIST_H
#define ERRORLIST_H

#include <QObject>

class ErrorListItem : public QObject
{
//http://doc.qt.io/archives/qt-4.8/qt-declarative-modelviews-objectlistmodel-dataobject-h.html
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(qint16 num READ num  WRITE setNum NOTIFY numChanged)

public:
    explicit ErrorListItem(QObject *parent = 0);
    ~ErrorListItem();

    QString name() const;
    Q_INVOKABLE void setName(const QString &name);

    QString text() const;
    Q_INVOKABLE void setText(const QString &text);

    qint16 num() const;
    Q_INVOKABLE void setNum(const qint16 &num);

signals:
    void nameChanged();
    void textChanged();
    void numChanged();
public slots:

private:
    QString     m_name;
    QString     m_text;
    qint16      m_num;

};

#endif // ERRORLIST_H
