import QtQuick 1.1
import "style/Style.js" as S
import tools 1.1
import tools.shortcut 1.1
import vectovision 1.1
import Analyser 1.0
import myplugins.exampleModule 1.0
import myplugins.elcede 1.0

FocusScope {
    id: mainrect
    objectName: "aradexRoot"
    smooth: S.isFast
    width: 640
    height: 480

    property real   originalWidth
    property real   originalHeight
    property real   thisScale
    property string rootStyle: S.styleName
	
	//	signal setResizeMode
    //  iMode=0: SizeRootObjectToView (standard behave, frame windows does not change size)
    //  iMode=1: SizeViewToRootObject ( framewindow changes size according to the size of the root object (mainrect)
    signal setResizeMode(int iMode)

    // move the main window
    signal move(int x, int y)
	
	// signal that is received from the viewer on moving for the virtual keyboard
	signal movedForKeyboard(int x, int y)
	
	// signal that is received in c++ code
	signal styleBackgroundColor(color colBackground)

    // signal that is received in c++ code on style change
    signal styleChanged(string newStyle)
    onStyleChanged: {
        rootStyle = newStyle
        styleBackgroundColor(S.Style[mainrect.rootStyle].backgroundColor)
    }
	
    // signal that is received in c++ code
    signal scaledSizeChanged(real minScale)
    onScaledSizeChanged: thisScale = minScale
    signal languageChanged()
    signal timerActivated()

    // this signal needs to be emitted if the style is to be changed from root object
    signal changeRootStyle(string newStyle)

    onRootStyleChanged:{
        styleChanged(rootStyle)
        var stateTemp = mainrect.state
        mainrect.state = ""
        mainrect.state = stateTemp
    }

    function clickRoot() {
        swipearea.forceActiveFocus()
    }
    Component.onCompleted: {
        originalWidth = width
        originalHeight = height
    }
	
	// signal that is emitted by the viewer app when the application is ready to run
    signal applicationStart()
    onApplicationStart: {
	}

    Rectangle{
        id: backgroundRectangle
        anchors.fill: parent
        color: S.Style[mainrect.rootStyle].backgroundColor
        x: -1


        SwipeArea{
            id: swipearea
            anchors.fill: parent
            onReleaseNoDrag: forceActiveFocus()
            onMove: mainrect.move(x,y)

            GroupBox {
                id: mainContainer
                anchors.top: titleContainer.bottom
                anchors.topMargin: 4
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 4
                anchors.left: chooseScreensGroup.right
                anchors.leftMargin: 8
                anchors.right: parent.right
                anchors.rightMargin: 4
                colorChoice: 1
                headerVisible: false
            }

            GroupBox {
                id: titleContainer
                y: 4
                height: 68
                anchors.right: corporateImage.left
                anchors.rightMargin: 12
                anchors.left: parent.left
                anchors.leftMargin: 4
                colorChoice: 0
                headerVisible: false

                Label {
                    id: titleLabelState0
                    y: 4
                    height: 60
                    anchors.left: parent.left
                    anchors.leftMargin: 4
                    anchors.right: parent.right
                    anchors.rightMargin: 4
                    colorChoice: 1
                    fontSize: 36
                    __verticalTextAlignement: Qt.AlignHCenter
                    text: "State0"
                    opacity: 0
                }
                                Label {
                    id: titleLabelState1
                    y: 4
                    height: 60
                    anchors.left: parent.left
                    anchors.leftMargin: 4
                    anchors.right: parent.right
                    anchors.rightMargin: 4
                    colorChoice: 1
                    fontSize: 36
                    __verticalTextAlignement: Qt.AlignHCenter
                    text: "State1"
                    opacity: 0
                }


// @titleLabel@
            }

            Image {
                id: corporateImage
                x: 384
                y: 18
                width: 252
                height: 41
                anchors.right: parent.right
                anchors.rightMargin: 4
                source: "images/Aradex_Logo.png"
            }

            RadioGroup {
                id: chooseScreensGroup
                x: 4
                y: 76
                width: 136
                height: 116
                anchors.leftMargin: 4
                anchors.topMargin: 4
                anchors.left: parent.left
                anchors.top: titleContainer.bottom
                initialState: "State0"
                stateTarget: "mainrect"

                RadioButton {
                    id: chooseScreenRadioButton0
                    text: "State0"
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked_changeStateTo: "State0"
                }

                RadioButton {
                    id: chooseScreenRadioButton1
                    text: "State1"
                    anchors.top: chooseScreenRadioButton0.bottom
                    anchors.topMargin: 4
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked_changeStateTo: "State1"
                }

// @RadioButton@
            }

            Rectangle {
                id: test
               // x: 170
               // y: 160
                height: parent.height - titleContainer.height - 24
                width: parent.width - chooseScreensGroup.width - 24
                anchors.top: titleContainer.bottom
                anchors.topMargin: 10
                anchors.left: chooseScreensGroup.right
                anchors.leftMargin: 12
                //color: "red"
/*
                MyComponent  {
                    id: myTestComponent
                    anchors.fill: parent
               }
*/
                DieCadViewer  {
                    id: myTestComponent
                    filename: "files:d:\\STLFiles\\Acc\\Bender.acc"
                    anchors.fill: parent
                }


            }
        }
    }

    states: [
        State {
            name: "State0"

            PropertyChanges {
                target: titleLabelState0
                opacity: 1
            }
        },
        State {
            name: "State1"

            PropertyChanges {
                target: titleLabelState1
                opacity: 1
            }
        }// @State@
    ]
}
