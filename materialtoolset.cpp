#include "materialtoolset.h"

MaterialToolset::MaterialToolset(QObject *parent) :
    QObject(parent)
{
}

int MaterialToolset::id() const
{
    return m_id;
}

void MaterialToolset::setId(const int iVal)
{
    m_id = iVal;
}

QString MaterialToolset::name() const
{
    return m_name;
}

void MaterialToolset::setName(const QString &name)
{
    m_name = name;
}

int MaterialToolset::mac_type() const
{
    return m_mac_type;
}

void MaterialToolset::setMac_Type(const int iVal)
{
    m_mac_type = iVal;
}
