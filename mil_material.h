#ifndef MILMATERIAL_H
#define MILMATERIAL_H

#include <QObject>
#include <QDeclarativeItem>
#include <QtSql>
#include "materialtool.h"


QT_FORWARD_DECLARE_CLASS(Database)

class MilMaterial : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name                              READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString strFile                           READ strFile WRITE setstrFile NOTIFY strFileChanged)
    Q_PROPERTY(double  dRawMaterialThickness             READ dRawMaterialThickness WRITE setdRawMaterialThickness NOTIFY dRawMaterialThicknessChanged)
    Q_PROPERTY(double  dSlopeTime                        READ dSlopeTime WRITE setdSlopeTime NOTIFY dSlopeTimeChanged)
    Q_PROPERTY(bool    DeleteTools_bActive               READ DeleteTools_bActive WRITE setDeleteTools_bActive NOTIFY DeleteTools_bActiveChanged)
    Q_PROPERTY(bool    OptimizeEmptyMoves_bActive        READ OptimizeEmptyMoves_bActive WRITE setOptimizeEmptyMoves_bActive NOTIFY OptimizeEmptyMoves_bActiveChanged)
    Q_PROPERTY(QString OptimizeEmptyMoves_strToolSequence READ OptimizeEmptyMoves_strToolSequence WRITE setOptimizeEmptyMoves_strToolSequence NOTIFY OptimizeEmptyMoves_strToolSequenceChanged)
    Q_PROPERTY(bool    bSteel                            READ bSteel WRITE setbSteel NOTIFY bSteelChanged)
    Q_PROPERTY(double  dCentripetalForceLimitFactor      READ dCentripetalForceLimitFactor WRITE setdCentripetalForceLimitFactor NOTIFY dCentripetalForceLimitFactorChanged)
    Q_PROPERTY(double  dAcceleration                     READ dAcceleration WRITE setdAcceleration NOTIFY dAccelerationChanged)
    Q_PROPERTY(double  dSupportMaterialThickness         READ dSupportMaterialThickness WRITE setdSupportMaterialThickness NOTIFY dSupportMaterialThicknessChanged)
    Q_PROPERTY(int     iType                             READ iType WRITE setiType NOTIFY iTypeChanged)
    Q_PROPERTY(int     mil_toolset                       READ mil_toolset WRITE setmil_toolset NOTIFY mil_toolsetChanged)

    Q_PROPERTY(QList<QObject*> tools                     READ tools NOTIFY toolsChanged)



public:
    explicit MilMaterial(Database *parent = 0);
    ~MilMaterial();

//copy constructor
//    Material(const Material &m);
//properties

    QString name() const;
    Q_INVOKABLE void setName(const QString&);


    QString strFile() const;
    Q_INVOKABLE void setstrFile(const QString&);

    bool DeleteTools_bActive() const;
    Q_INVOKABLE void setDeleteTools_bActive(bool);

    bool OptimizeEmptyMoves_bActive() const;
    Q_INVOKABLE void setOptimizeEmptyMoves_bActive(bool);

    QString OptimizeEmptyMoves_strToolSequence() const;
    Q_INVOKABLE void setOptimizeEmptyMoves_strToolSequence(const QString&);

    bool bSteel() const;
    Q_INVOKABLE void setbSteel(bool);

    double dRawMaterialThickness() const;
    Q_INVOKABLE void setdRawMaterialThickness(double);

    double dSlopeTime() const;
    Q_INVOKABLE void setdSlopeTime(double);

    double dCentripetalForceLimitFactor() const;
    Q_INVOKABLE void setdCentripetalForceLimitFactor(double);

    double dAcceleration() const;
    Q_INVOKABLE void setdAcceleration(double);

    double dSupportMaterialThickness() const;
    Q_INVOKABLE void setdSupportMaterialThickness(double);

    int iType() const;
    Q_INVOKABLE void setiType(int);

    int mil_toolset() const;
    Q_INVOKABLE void setmil_toolset(int);


    QList<QObject*> tools() const;

    void fetchTools();
    Q_INVOKABLE void update();
    Q_INVOKABLE bool fetch();
    Q_INVOKABLE int  copy(const QString);
    Q_INVOKABLE bool rename(const QString, const QString);
    Q_INVOKABLE bool deleteMat(const QString);
    Q_INVOKABLE int  newMat(const QString);
    Q_INVOKABLE bool isValidName(const QString);

    void setDatabase(QSqlDatabase*);


signals:
    void nameChanged();
    void strFileChanged();
    void DeleteTools_bActiveChanged();
    void OptimizeEmptyMoves_bActiveChanged();
    void OptimizeEmptyMoves_strToolSequenceChanged();
    void bSteelChanged();
    void dRawMaterialThicknessChanged();
    void dSlopeTimeChanged();
    void dCentripetalForceLimitFactorChanged();
    void dAccelerationChanged();
    void dSupportMaterialThicknessChanged();
    void toolsChanged();
    void iTypeChanged();
    void mil_toolsetChanged();


public slots:


private:

    QString         m_name;
    QSqlDatabase*   m_db;
    int             m_id;
    QString         m_strFile;
    bool            m_DeleteTools_bActive;
    bool            m_OptimizeEmptyMoves_bActive;
    QString         m_OptimizeEmptyMoves_strToolSequence;
    bool            m_bSteel;
    double          m_dRawMaterialThickness;
    double          m_dSlopeTime;
    double          m_dCentripetalForceLimitFactor;
    double          m_dAcceleration;
    double          m_dSupportMaterialThickness;
    int             m_iType;
    int             m_mil_toolset;

    int             m_iMaxTools;
    Database*       m_Database;

    QList<QObject*> m_tools;

};

#endif // MILMATERIAL_H
