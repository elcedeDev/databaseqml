#ifndef DATABASE_PLUGIN_H
#define DATABASE_PLUGIN_H

#include <QDeclarativeExtensionPlugin>

class DatabasePlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "Database")
#endif

public:
    void registerTypes(const char *uri);
};

#endif // DATABASE_PLUGIN_H
