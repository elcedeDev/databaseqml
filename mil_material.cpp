#include "mil_material.h"
#include "database.h"
#include "mil_materialtool.h"

MilMaterial::MilMaterial(Database *parent) :
    QObject((QDeclarativeItem*)parent),
    m_Database(parent)
{

    m_iMaxTools = 20;
    m_id = 0;
    for(int i = 0; i <= m_iMaxTools; ++i)
    {
        MilMaterialTool* mt = new MilMaterialTool();
        mt->setmil_toolset(m_mil_toolset);
        mt->setnum(i);
        m_tools.append(mt);
    }
}

MilMaterial::~MilMaterial()
{
    m_tools.clear();
}

QString MilMaterial::name() const
{
    return m_strFile;
}

void MilMaterial::setName(const QString &str)
{
    m_strFile = str;
}

QString MilMaterial::strFile() const
{
    return m_strFile;
}

void MilMaterial::setstrFile(const QString& str)
{
    m_strFile = str;
}

bool MilMaterial::DeleteTools_bActive() const
{
    return m_DeleteTools_bActive;
}

void MilMaterial::setDeleteTools_bActive(bool b)
{
    m_DeleteTools_bActive = b;
}

bool MilMaterial::OptimizeEmptyMoves_bActive() const
{
    return m_OptimizeEmptyMoves_bActive;
}

void MilMaterial::setOptimizeEmptyMoves_bActive(bool b)
{
    m_OptimizeEmptyMoves_bActive = b;
}

QString MilMaterial::OptimizeEmptyMoves_strToolSequence() const
{
    return m_OptimizeEmptyMoves_strToolSequence;
}

void MilMaterial::setOptimizeEmptyMoves_strToolSequence(const QString &str)
{
    m_OptimizeEmptyMoves_strToolSequence = str;
}

bool MilMaterial::bSteel() const
{
    return m_bSteel;
}

void MilMaterial::setbSteel(bool b)
{
    m_bSteel = b;
}

double MilMaterial::dRawMaterialThickness() const
{
    return m_dRawMaterialThickness;
}

void MilMaterial::setdRawMaterialThickness(double d)
{
    m_dRawMaterialThickness = d;
}

double MilMaterial::dSlopeTime() const
{
    return m_dSlopeTime;
}

void MilMaterial::setdSlopeTime(double d)
{
    m_dSlopeTime = d;
}

double MilMaterial::dCentripetalForceLimitFactor() const
{
    return m_dCentripetalForceLimitFactor;
}

void MilMaterial::setdCentripetalForceLimitFactor(double d)
{
    m_dCentripetalForceLimitFactor = d;
}


double MilMaterial::dAcceleration() const
{
    return m_dAcceleration;
}

void MilMaterial::setdAcceleration(double dVal)
{
    m_dAcceleration = dVal;
}

double MilMaterial::dSupportMaterialThickness() const
{
    return m_dSupportMaterialThickness;
}

void MilMaterial::setdSupportMaterialThickness(double dVal)
{
    m_dSupportMaterialThickness = dVal;
}

int MilMaterial::iType() const
{
    return m_iType;
}

void MilMaterial::setiType(int iVal)
{
    m_iType = iVal;
}


QList<QObject *> MilMaterial::tools() const
{
    return m_tools;
}

void MilMaterial::fetchTools()
{
    m_tools.clear();
    for(int i = 0; i < m_iMaxTools; ++i)
    {
        MilMaterialTool* mt = new MilMaterialTool();
        mt->setmil_toolset(m_mil_toolset);
        mt->setnum(i);
        mt->fetch(m_db);
        m_tools.append(mt);
    }
}

void MilMaterial::update()
{
    m_db->transaction();
    QSqlQuery q(*m_db);
        QString strSQL = QString("SELECT update_mil_material(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

        q.prepare(strSQL);
        int i = 0;
        q.bindValue(i, m_strFile); ++i;
        q.bindValue(i, m_dRawMaterialThickness); ++i;
        q.bindValue(i, m_dSlopeTime); ++i;
        q.bindValue(i, m_DeleteTools_bActive); ++i;
        q.bindValue(i, m_OptimizeEmptyMoves_bActive); ++i;
        q.bindValue(i, m_OptimizeEmptyMoves_strToolSequence); ++i;
        q.bindValue(i, m_bSteel); ++i;
        q.bindValue(i, m_dCentripetalForceLimitFactor); ++i;
        q.bindValue(i, m_dAcceleration); ++i;
        q.bindValue(i, m_dSupportMaterialThickness); ++i;
        q.bindValue(i, m_iType); ++i;
        q.bindValue(i, m_mil_toolset);
        q.exec();

        //qDebug() << "Last executed query:" << getLastExecutedQuery(q);

        if(q.first())
        {
            m_id = q.value(0).toInt();

            int j = 0;
            for(auto i = m_tools.begin(); i != m_tools.end(); ++i, ++j)
            {
                ((MilMaterialTool*)(*i))->setmil_toolset(m_mil_toolset);
                if(((MilMaterialTool*)(*i))->update(m_db) == -1)
                    qDebug() << "ERROR -- MilMaterial::update() - tool->update - mat_id:" << m_id << "tool num:" << j;
            }

        }
        else
        {
            QString str;
            QTextStream out(&str);
            out << "ERROR -- MilMaterial::update() ----- " << q.lastError().text();
            //qDebug() << "ERROR -- MilMaterial::update() ----- " << q.lastError();
            qDebug() << str;
            m_Database->setLasterror(str);
            m_db->rollback();
        }

        m_db->commit();
}

bool MilMaterial::fetch()
{

    QSqlQuery q(*m_db);
    QString strSQL = QString("SELECT * FROM mil_material WHERE \"strFile\" = '%1';").arg(m_strFile);

    q.prepare(strSQL);

    q.exec();

    int i = 0;

    if(q.first())
    {
        m_id                                   = q.value(i).toInt();  ++i;
        m_strFile                              = q.value(i).toString();  ++i;
        m_dRawMaterialThickness                = q.value(i).toDouble();  ++i;
        m_dSlopeTime                           = q.value(i).toDouble();  ++i;
        m_DeleteTools_bActive                  = q.value(i).toBool();  ++i;
        m_OptimizeEmptyMoves_bActive           = q.value(i).toBool();  ++i;
        m_OptimizeEmptyMoves_strToolSequence   = q.value(i).toString(); ++i;
        m_bSteel                               = q.value(i).toBool();  ++i;
        m_dCentripetalForceLimitFactor         = q.value(i).toDouble(); ++i;
        m_dAcceleration                        = q.value(i).toDouble(); ++i;
        m_dSupportMaterialThickness            = q.value(i).toDouble(); ++i;
        m_iType                                = q.value(i).toInt();  ++i;
        m_mil_toolset                          = q.value(i).toInt();
        fetchTools();
        return true;

    }else{
        QString str;
        QTextStream out(&str);
        out  << "ERROR fetching mil_material: " << m_strFile << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        //qDebug() << "ERROR fetching mil_material: " << m_strFile << q.lastError();
        return false;
    }
}

int MilMaterial::copy(const QString strName)
{
    QSqlQuery q(*m_db);
    QString strSQL = QString("SELECT copy_mil_material(?);");
    q.prepare(strSQL);
    q.bindValue(0, strName);

    q.exec();

    if(q.first())
    {
        //qDebug() << "SUCCESS: --- int MilMaterial::copy(const QString strName)" << strName;
        return q.value(0).toInt();
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        //qDebug() << "ERROR: cannot be executed: " << strSQL << q.lastError();
        return 0;
    }

}

bool MilMaterial::rename(const QString strOld, const QString strNew)
{
    if(! isValidName(strNew)) return false;

    QSqlQuery q(*m_db);
    QString strSQL = QString(" UPDATE mil_material SET \"strFile\"= :newname WHERE \"strFile\"= :oldname returning id;");
    q.prepare(strSQL);
    q.bindValue(0, strNew);
    q.bindValue(1, strOld);

    q.exec();

    if(q.first())
        return true;
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << "  <0:>" << strNew << "  <1:>" << strOld << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        return false;
    }

}

bool MilMaterial::deleteMat(const QString strName)
{
    QSqlQuery q(*m_db);
    QString strSQL = QString("DELETE FROM mil_material WHERE \"strFile\"= :strMat returning id;");
    q.prepare(strSQL);
    q.bindValue(0, strName);

    q.exec();

    if(q.first())
    {
        m_Database->setLasterror("");
        //qDebug() << "MilMaterial::deleteMat -- DELETED: " << strName;
        return true;
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text() << strName;
        qDebug() << str;
        m_Database->setLasterror(str);
        return false;
    }
}

int MilMaterial::newMat(const QString strName)
{
    m_db->transaction();
    QSqlQuery q(*m_db);
    QString strSQL = QString("INSERT INTO mil_material(\"strFile\") VALUES(:strMat) returning id;");
    q.prepare(strSQL);
    q.bindValue(0, strName);

    q.exec();

    if(q.first())
    {
        m_id    = q.value(0).toInt();

        QSqlQuery q2(*m_db);
        strSQL = QString("INSERT INTO mil_material_tool(mat_id, num) VALUES(:matid, :numval) returning num;");
        q2.prepare(strSQL);

        for(int i = 0; i < 41; ++i)
        {
            q2.bindValue(0, m_id);
            q2.bindValue(1, i);

            if(!q2.exec()) {
                QString str;
                QTextStream out(&str);
                out  << "ERROR: cannot be executed: " << strSQL << q2.lastError().text();
                qDebug() << str;
                m_Database->setLasterror(str);
                m_db->rollback();
                return -i;
            }
        }
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);
        m_db->rollback();
        return 0;
    }
    m_db->commit();
    return m_id;

}

bool MilMaterial::isValidName(const QString strName)
{
    if(!strName.length()) return false;

    QSqlQuery q(*m_db);
    QString strSQL = QString("SELECT count(*) FROM mil_material WHERE \"strFile\"= :strMat;");
    q.prepare(strSQL);
    q.bindValue(0, strName);
    q.exec();

    if(q.first())
    {
        //qDebug() << "bool Material::isValidName(const QString strName)" << strName << "q.value(0).toBool()" << q.value(0).toBool() << "q.value(0).toInt()" << q.value(0).toInt();
        return !(q.value(0).toBool());
    }
    else
    {
        QString str;
        QTextStream out(&str);
        out  << "ERROR: cannot be executed: " << strSQL << q.lastError().text();
        qDebug() << str;
        m_Database->setLasterror(str);

        return false;
    }
}

void MilMaterial::setDatabase(QSqlDatabase *db)
{
    m_db = db;
}

