#ifndef MATERIALTOOL_H
#define MATERIALTOOL_H

#include <QObject>
#include <QtSql>

class MaterialTool : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int las_toolset READ las_toolset WRITE setlas_toolset NOTIFY las_toolsetChanged)
    Q_PROPERTY(int num READ num WRITE setnum NOTIFY numChanged)
    Q_PROPERTY(bool bBoarderOnly READ bBoarderOnly WRITE setbBoarderOnly NOTIFY bBoarderOnlyChanged)
    Q_PROPERTY(bool bDelete READ bDelete WRITE setbDelete NOTIFY bDeleteChanged)
    Q_PROPERTY(bool bEngravingTool READ bEngravingTool WRITE setbEngravingTool NOTIFY bEngravingToolChanged)
    Q_PROPERTY(bool bSlotShortEdges READ bSlotShortEdges WRITE setbSlotShortEdges NOTIFY bSlotShortEdgesChanged)
    Q_PROPERTY(bool bSlotSmoothenEdges READ bSlotSmoothenEdges WRITE setbSlotSmoothenEdges NOTIFY bSlotSmoothenEdgesChanged)
    Q_PROPERTY(bool bSlotTurnClockwise READ bSlotTurnClockwise WRITE setbSlotTurnClockwise NOTIFY bSlotTurnClockwiseChanged)
    Q_PROPERTY(bool bZMillCompensation READ bZMillCompensation WRITE setbZMillCompensation NOTIFY bZMillCompensationChanged)
    Q_PROPERTY(double dAccelerationMax READ dAccelerationMax WRITE setdAccelerationMax NOTIFY dAccelerationMaxChanged)
    Q_PROPERTY(double dAccLsrFrequency READ dAccLsrFrequency WRITE setdAccLsrFrequency NOTIFY dAccLsrFrequencyChanged)
    Q_PROPERTY(double dAccLsrPulseWidth READ dAccLsrPulseWidth WRITE setdAccLsrPulseWidth NOTIFY dAccLsrPulseWidthChanged)
    Q_PROPERTY(double dAnalogOutput READ dAnalogOutput WRITE setdAnalogOutput NOTIFY dAnalogOutputChanged)
    Q_PROPERTY(double dAngle READ dAngle WRITE setdAngle NOTIFY dAngleChanged)
    Q_PROPERTY(double dBeamRotation READ dBeamRotation WRITE setdBeamRotation NOTIFY dBeamRotationChanged)
    Q_PROPERTY(double dChannelWidth READ dChannelWidth WRITE setdChannelWidth NOTIFY dChannelWidthChanged)
    Q_PROPERTY(double dConstLsrFrequency READ dConstLsrFrequency WRITE setdConstLsrFrequency NOTIFY dConstLsrFrequencyChanged)
    Q_PROPERTY(double dConstLsrPulseWidth READ dConstLsrPulseWidth WRITE setdConstLsrPulseWidth NOTIFY dConstLsrPulseWidthChanged)
    Q_PROPERTY(double dDeccLsrFrequency READ dDeccLsrFrequency WRITE setdDeccLsrFrequency NOTIFY dDeccLsrFrequencyChanged)
    Q_PROPERTY(double dDeccLsrPulseWidth READ dDeccLsrPulseWidth WRITE setdDeccLsrPulseWidth NOTIFY  dDeccLsrPulseWidthChanged)
    Q_PROPERTY(double dDiameter READ dDiameter WRITE setdDiameter NOTIFY  dDiameterChanged)
    Q_PROPERTY(double dDistanceDepth READ dDistanceDepth WRITE setdDistanceDepth NOTIFY  dDistanceDepthChanged)
    Q_PROPERTY(double dEnvelopeCut READ dEnvelopeCut WRITE setdEnvelopeCut NOTIFY  dEnvelopeCutChanged)
    Q_PROPERTY(double dHeightXm READ dHeightXm WRITE setdHeightXm NOTIFY  dHeightXmChanged)
    Q_PROPERTY(double dHeightXmYm READ dHeightXmYm WRITE setdHeightXmYm NOTIFY  dHeightXmYmChanged)
    Q_PROPERTY(double dHeightXmYp READ dHeightXmYp WRITE setdHeightXmYp NOTIFY  dHeightXmYpChanged)
    Q_PROPERTY(double dHeightXp READ dHeightXp WRITE setdHeightXp NOTIFY  dHeightXpChanged)
    Q_PROPERTY(double dHeightXpYm READ dHeightXpYm WRITE setdHeightXpYm NOTIFY  dHeightXpYmChanged)
    Q_PROPERTY(double dHeightXpYp READ dHeightXpYp WRITE setdHeightXpYp NOTIFY  dHeightXpYpChanged)
    Q_PROPERTY(double dHeightYm READ dHeightYm WRITE setdHeightYm NOTIFY  dHeightYmChanged)
    Q_PROPERTY(double dHeightYp READ dHeightYp WRITE setdHeightYp NOTIFY  dHeightYpChanged)
    Q_PROPERTY(double dLookAheadLimitAngleMin READ dLookAheadLimitAngleMin WRITE setdLookAheadLimitAngleMin NOTIFY  dLookAheadLimitAngleMinChanged)
    Q_PROPERTY(double dLookAheadLinitAngleMax READ dLookAheadLinitAngleMax WRITE setdLookAheadLinitAngleMax NOTIFY  dLookAheadLinitAngleMaxChanged)
    Q_PROPERTY(double dLsrAnalogPower READ dLsrAnalogPower WRITE setdLsrAnalogPower NOTIFY  dLsrAnalogPowerChanged)
    Q_PROPERTY(double dLsrExponentAnalogPower READ dLsrExponentAnalogPower WRITE setdLsrExponentAnalogPower NOTIFY  dLsrExponentAnalogPowerChanged)
    Q_PROPERTY(double dLsrExponentFrequency READ dLsrExponentFrequency WRITE setdLsrExponentFrequency NOTIFY  dLsrExponentFrequencyChanged)
    Q_PROPERTY(double dLsrFactorAnalogPower READ dLsrFactorAnalogPower WRITE setdLsrFactorAnalogPower NOTIFY  dLsrFactorAnalogPowerChanged)
    Q_PROPERTY(double dLsrFactorFrequency READ dLsrFactorFrequency WRITE setdLsrFactorFrequency NOTIFY dLsrFactorFrequencyChanged)
    Q_PROPERTY(double dMaxDeviation READ dMaxDeviation WRITE setdMaxDeviation NOTIFY dMaxDeviationChanged)
    Q_PROPERTY(double dMaxPulseWidthSpeed READ dMaxPulseWidthSpeed WRITE setdMaxPulseWidthSpeed NOTIFY dMaxPulseWidthSpeedChanged)
    Q_PROPERTY(double dPiercingSpeed READ dPiercingSpeed WRITE setdPiercingSpeed NOTIFY dPiercingSpeedChanged)
    Q_PROPERTY(double dSpeedMax READ dSpeedMax WRITE setdSpeedMax NOTIFY dSpeedMaxChanged)
    Q_PROPERTY(double dSpindleSpeed READ dSpindleSpeed WRITE setdSpindleSpeed NOTIFY dSpindleSpeedChanged)
    Q_PROPERTY(double dStartMacroAnalogPower READ dStartMacroAnalogPower WRITE setdStartMacroAnalogPower NOTIFY dStartMacroAnalogPowerChanged)
    Q_PROPERTY(double dStartMacroLsrFrequency READ dStartMacroLsrFrequency WRITE setdStartMacroLsrFrequency NOTIFY dStartMacroLsrFrequencyChanged)
    Q_PROPERTY(double dStartMacroLsrPulseWidth READ dStartMacroLsrPulseWidth WRITE setdStartMacroLsrPulseWidth NOTIFY dStartMacroLsrPulseWidthChanged)
    Q_PROPERTY(double dStartMacroTime READ dStartMacroTime WRITE setdStartMacroTime NOTIFY dStartMacroTimeChanged)
    Q_PROPERTY(double dStopMacroTime READ dStopMacroTime WRITE setdStopMacroTime NOTIFY dStopMacroTimeChanged)
    Q_PROPERTY(double dStopMacroAnalogPower READ dStopMacroAnalogPower WRITE setdStopMacroAnalogPower NOTIFY dStopMacroAnalogPowerChanged)
    Q_PROPERTY(double dStopMacroLsrFrequency READ dStopMacroLsrFrequency WRITE setdStopMacroLsrFrequency NOTIFY dStopMacroLsrFrequencyChanged)
    Q_PROPERTY(double dStopMacroLsrPulseWidth READ dStopMacroLsrPulseWidth WRITE setdStopMacroLsrPulseWidth NOTIFY dStopMacroLsrPulseWidthChanged)
    Q_PROPERTY(double dToolTest READ dToolTest WRITE setdToolTest NOTIFY dToolTestChanged)
    Q_PROPERTY(double dWorkHeight READ dWorkHeight WRITE setdWorkHeight NOTIFY dWorkHeightChanged)
    Q_PROPERTY(double dWorkSpeedXm READ dWorkSpeedXm WRITE setdWorkSpeedXm NOTIFY dWorkSpeedXmChanged)
    Q_PROPERTY(double dWorkSpeedXmYm READ dWorkSpeedXmYm WRITE setdWorkSpeedXmYm NOTIFY dWorkSpeedXmYmChanged)
    Q_PROPERTY(double dWorkSpeedXmYp READ dWorkSpeedXmYp WRITE setdWorkSpeedXmYp NOTIFY dWorkSpeedXmYpChanged)
    Q_PROPERTY(double dWorkSpeedXp READ dWorkSpeedXp WRITE setdWorkSpeedXp NOTIFY dWorkSpeedXpChanged)
    Q_PROPERTY(double dWorkSpeedXpYm READ dWorkSpeedXpYm WRITE setdWorkSpeedXpYm NOTIFY dWorkSpeedXpYmChanged)
    Q_PROPERTY(double dWorkSpeedXpYp READ dWorkSpeedXpYp WRITE setdWorkSpeedXpYp NOTIFY dWorkSpeedXpYpChanged)
    Q_PROPERTY(double dWorkSpeedYm READ dWorkSpeedYm WRITE setdWorkSpeedYm NOTIFY dWorkSpeedYmChanged)
    Q_PROPERTY(double dWorkSpeedYp READ dWorkSpeedYp WRITE setdWorkSpeedYp NOTIFY dWorkSpeedYpChanged)
    Q_PROPERTY(int iControlType READ iControlType WRITE setiControlType NOTIFY iControlTypeChanged)
    Q_PROPERTY(int iPocketTool READ iPocketTool WRITE setiPocketTool NOTIFY iPocketToolChanged)
    Q_PROPERTY(int iPocketType READ iPocketType WRITE setiPocketType NOTIFY iPocketTypeChanged)
    Q_PROPERTY(int iType READ iType WRITE setiType NOTIFY iTypeChanged)
    Q_PROPERTY(QString strNote READ strNote WRITE setstrNote NOTIFY strNoteChanged)
    Q_PROPERTY(bool ToolTest_bActive READ ToolTest_bActive WRITE setToolTest_bActive NOTIFY  ToolTest_bActiveChanged)
    Q_PROPERTY(int iToolType READ iToolType WRITE setiToolType NOTIFY iToolTypeChanged)
    Q_PROPERTY(bool bPulseOn READ bPulseOn WRITE setbPulseOn NOTIFY bPulseOnChanged)
    Q_PROPERTY(double dLsrSpotDistance READ dLsrSpotDistance WRITE setdLsrSpotDistance NOTIFY dLsrSpotDistanceChanged)

public:
    explicit MaterialTool(QObject *parent = 0);

    int las_toolset() const;
    Q_INVOKABLE void setlas_toolset(const int);

    int num() const;
    Q_INVOKABLE void setnum(const int);

    bool bBoarderOnly() const;
    Q_INVOKABLE void setbBoarderOnly(const bool);

    bool bDelete() const;
    Q_INVOKABLE void setbDelete(const bool);

    bool bEngravingTool() const;
    Q_INVOKABLE void setbEngravingTool(const bool);

    bool bSlotShortEdges() const;
    Q_INVOKABLE void setbSlotShortEdges(const bool);

    bool bSlotSmoothenEdges() const;
    Q_INVOKABLE void setbSlotSmoothenEdges(const bool);

    bool bSlotTurnClockwise() const;
    Q_INVOKABLE void setbSlotTurnClockwise(const bool);

    bool bZMillCompensation() const;
    Q_INVOKABLE void setbZMillCompensation(const bool);

    double dAccelerationMax() const;
    Q_INVOKABLE void setdAccelerationMax(const double);

    double dAccLsrFrequency() const;
    Q_INVOKABLE void setdAccLsrFrequency(const double);

    double dAccLsrPulseWidth() const;
    Q_INVOKABLE void setdAccLsrPulseWidth(const double);

    double dAnalogOutput() const;
    Q_INVOKABLE void setdAnalogOutput(const double);

    double dAngle() const;
    Q_INVOKABLE void setdAngle(const double);

    double dBeamRotation() const;
    Q_INVOKABLE void setdBeamRotation(const double);

    double dChannelWidth() const;
    Q_INVOKABLE void setdChannelWidth(const double);

    double dConstLsrFrequency() const;
    Q_INVOKABLE void setdConstLsrFrequency(const double);

    double dConstLsrPulseWidth() const;
    Q_INVOKABLE void setdConstLsrPulseWidth(const double);

    double dDeccLsrFrequency() const;
    Q_INVOKABLE void setdDeccLsrFrequency(const double);

    double dDeccLsrPulseWidth() const;
    Q_INVOKABLE void setdDeccLsrPulseWidth(const double);

    double dDiameter() const;
    Q_INVOKABLE void setdDiameter(const double);

    double dDistanceDepth() const;
    Q_INVOKABLE void setdDistanceDepth(const double);

    double dEnvelopeCut() const;
    Q_INVOKABLE void setdEnvelopeCut(const double);

    double dHeightXm() const;
    Q_INVOKABLE void setdHeightXm(const double);

    double dHeightXmYm() const;
    Q_INVOKABLE void setdHeightXmYm(const double);

    double dHeightXmYp() const;
    Q_INVOKABLE void setdHeightXmYp(const double);

    double dHeightXp() const;
    Q_INVOKABLE void setdHeightXp(const double);

    double dHeightXpYm() const;
    Q_INVOKABLE void setdHeightXpYm(const double);

    double dHeightXpYp() const;
    Q_INVOKABLE void setdHeightXpYp(const double);

    double dHeightYm() const;
    Q_INVOKABLE void setdHeightYm(const double);

    double dHeightYp() const;
    Q_INVOKABLE void setdHeightYp(const double);

    double dLookAheadLimitAngleMin() const;
    Q_INVOKABLE void setdLookAheadLimitAngleMin(const double);

    double dLookAheadLinitAngleMax() const;
    Q_INVOKABLE void setdLookAheadLinitAngleMax(const double);

    double dLsrAnalogPower() const;
    Q_INVOKABLE void setdLsrAnalogPower(const double);

    double dLsrExponentAnalogPower() const;
    Q_INVOKABLE void setdLsrExponentAnalogPower(const double);

    double dLsrExponentFrequency() const;
    Q_INVOKABLE void setdLsrExponentFrequency(const double);

    double dLsrFactorAnalogPower() const;
    Q_INVOKABLE void setdLsrFactorAnalogPower(const double);

    double dLsrFactorFrequency() const;
    Q_INVOKABLE void setdLsrFactorFrequency(const double);

    double dMaxDeviation() const;
    Q_INVOKABLE void setdMaxDeviation(const double);

    double dMaxPulseWidthSpeed() const;
    Q_INVOKABLE void setdMaxPulseWidthSpeed(const double);

    double dPiercingSpeed() const;
    Q_INVOKABLE void setdPiercingSpeed(const double);

    double dSpeedMax() const;
    Q_INVOKABLE void setdSpeedMax(const double);

    double dSpindleSpeed() const;
    Q_INVOKABLE void setdSpindleSpeed(const double);

    double dStartMacroAnalogPower() const;
    Q_INVOKABLE void setdStartMacroAnalogPower(const double);

    double dStartMacroLsrFrequency() const;
    Q_INVOKABLE void setdStartMacroLsrFrequency(const double);

    double dStartMacroLsrPulseWidth() const;
    Q_INVOKABLE void setdStartMacroLsrPulseWidth(const double);

    double dStartMacroTime() const;
    Q_INVOKABLE void setdStartMacroTime(const double);

    double dStopMacroTime() const;
    Q_INVOKABLE void setdStopMacroTime(const double);

    double dStopMacroAnalogPower() const;
    Q_INVOKABLE void setdStopMacroAnalogPower(const double);

    double dStopMacroLsrFrequency() const;
    Q_INVOKABLE void setdStopMacroLsrFrequency(const double);

    double dStopMacroLsrPulseWidth() const;
    Q_INVOKABLE void setdStopMacroLsrPulseWidth(const double);

    double dToolTest() const;
    Q_INVOKABLE void setdToolTest(const double);

    double dWorkHeight() const;
    Q_INVOKABLE void setdWorkHeight(const double);

    double dWorkSpeedXm() const;
    Q_INVOKABLE void setdWorkSpeedXm(const double);

    double dWorkSpeedXmYm() const;
    Q_INVOKABLE void setdWorkSpeedXmYm(const double);

    double dWorkSpeedXmYp() const;
    Q_INVOKABLE void setdWorkSpeedXmYp(const double);

    double dWorkSpeedXp() const;
    Q_INVOKABLE void setdWorkSpeedXp(const double);

    double dWorkSpeedXpYm() const;
    Q_INVOKABLE void setdWorkSpeedXpYm(const double);

    double dWorkSpeedXpYp() const;
    Q_INVOKABLE void setdWorkSpeedXpYp(const double);

    double dWorkSpeedYm() const;
    Q_INVOKABLE void setdWorkSpeedYm(const double);

    double dWorkSpeedYp() const;
    Q_INVOKABLE void setdWorkSpeedYp(const double);

    int     iControlType() const;
    Q_INVOKABLE void setiControlType(const int);

    int     iPocketTool() const;
    Q_INVOKABLE void setiPocketTool(const int);

    int     iPocketType() const;
    Q_INVOKABLE void setiPocketType(const int);

    int     iType() const;
    Q_INVOKABLE void setiType(const int);

    QString strNote() const;
    Q_INVOKABLE void setstrNote(const QString&);

    bool    ToolTest_bActive() const;
    Q_INVOKABLE void setToolTest_bActive(const bool);

    int     iToolType() const;
    Q_INVOKABLE void setiToolType(const int);

    bool    bPulseOn() const;
    Q_INVOKABLE void setbPulseOn(const bool);

    double dLsrSpotDistance() const;
    Q_INVOKABLE void setdLsrSpotDistance(const double);

    bool fetch(QSqlDatabase*);
    int update(QSqlDatabase*);


signals:
    void las_toolsetChanged();
    void numChanged();
    void bBoarderOnlyChanged();
    void bDeleteChanged();
    void bEngravingToolChanged();
    void bSlotShortEdgesChanged();
    void bSlotSmoothenEdgesChanged();
    void bSlotTurnClockwiseChanged();
    void bZMillCompensationChanged();
    void dAccelerationMaxChanged();
    void dAccLsrFrequencyChanged();
    void dAccLsrPulseWidthChanged();
    void dAnalogOutputChanged();
    void dAngleChanged();
    void dBeamRotationChanged();
    void dChannelWidthChanged();
    void dConstLsrFrequencyChanged();
    void dConstLsrPulseWidthChanged();
    void dDeccLsrFrequencyChanged();
    void dDeccLsrPulseWidthChanged();
    void dDiameterChanged();
    void dDistanceDepthChanged();
    void dEnvelopeCutChanged();
    void dHeightXmChanged();
    void dHeightXmYmChanged();
    void dHeightXmYpChanged();
    void dHeightXpChanged();
    void dHeightXpYmChanged();
    void dHeightXpYpChanged();
    void dHeightYmChanged();
    void dHeightYpChanged();
    void dLookAheadLimitAngleMinChanged();
    void dLookAheadLinitAngleMaxChanged();
    void dLsrAnalogPowerChanged();
    void dLsrExponentAnalogPowerChanged();
    void dLsrExponentFrequencyChanged();
    void dLsrFactorAnalogPowerChanged();
    void dLsrFactorFrequencyChanged();
    void dMaxDeviationChanged();
    void dMaxPulseWidthSpeedChanged();
    void dPiercingSpeedChanged();
    void dSpeedMaxChanged();
    void dSpindleSpeedChanged();
    void dStartMacroAnalogPowerChanged();
    void dStartMacroLsrFrequencyChanged();
    void dStartMacroLsrPulseWidthChanged();
    void dStartMacroTimeChanged();
    void dStopMacroTimeChanged();
    void dStopMacroAnalogPowerChanged();
    void dStopMacroLsrFrequencyChanged();
    void dStopMacroLsrPulseWidthChanged();
    void dToolTestChanged();
    void dWorkHeightChanged();
    void dWorkSpeedXmChanged();
    void dWorkSpeedXmYmChanged();
    void dWorkSpeedXmYpChanged();
    void dWorkSpeedXpChanged();
    void dWorkSpeedXpYmChanged();
    void dWorkSpeedXpYpChanged();
    void dWorkSpeedYmChanged();
    void dWorkSpeedYpChanged();
    void iControlTypeChanged();
    void iPocketToolChanged();
    void iPocketTypeChanged();
    void iTypeChanged();
    void strNoteChanged();
    void ToolTest_bActiveChanged();
    void iToolTypeChanged();
    void bPulseOnChanged();
    void dLsrSpotDistanceChanged();
public slots:

private:
    int     m_las_toolset;
    int     m_num;
    bool    m_bBoarderOnly;
    bool    m_bDelete;
    bool    m_bEngravingTool;
    bool    m_bSlotShortEdges;
    bool    m_bSlotSmoothenEdges;
    bool    m_bSlotTurnClockwise;
    bool    m_bZMillCompensation;
    double  m_dAccelerationMax;
    double  m_dAccLsrFrequency;
    double  m_dAccLsrPulseWidth;
    double  m_dAnalogOutput;
    double  m_dAngle;
    double  m_dBeamRotation;
    double  m_dChannelWidth;
    double  m_dConstLsrFrequency;
    double  m_dConstLsrPulseWidth;
    double  m_dDeccLsrFrequency;
    double  m_dDeccLsrPulseWidth;
    double  m_dDiameter;
    double  m_dDistanceDepth;
    double  m_dEnvelopeCut;
    double  m_dHeightXm;
    double  m_dHeightXmYm;
    double  m_dHeightXmYp;
    double  m_dHeightXp;
    double  m_dHeightXpYm;
    double  m_dHeightXpYp;
    double  m_dHeightYm;
    double  m_dHeightYp;
    double  m_dLookAheadLimitAngleMin;
    double  m_dLookAheadLinitAngleMax;
    double  m_dLsrAnalogPower;
    double  m_dLsrExponentAnalogPower;
    double  m_dLsrExponentFrequency;
    double  m_dLsrFactorAnalogPower;
    double  m_dLsrFactorFrequency;
    double  m_dMaxDeviation;
    double  m_dMaxPulseWidthSpeed;
    double  m_dPiercingSpeed;
    double  m_dSpeedMax;
    double  m_dSpindleSpeed;
    double  m_dStartMacroAnalogPower;
    double  m_dStartMacroLsrFrequency;
    double  m_dStartMacroLsrPulseWidth;
    double  m_dStartMacroTime;
    double  m_dStopMacroTime;
    double  m_dStopMacroAnalogPower;
    double  m_dStopMacroLsrFrequency;
    double  m_dStopMacroLsrPulseWidth;
    double  m_dToolTest;
    double  m_dWorkHeight;
    double  m_dWorkSpeedXm;
    double  m_dWorkSpeedXmYm;
    double  m_dWorkSpeedXmYp;
    double  m_dWorkSpeedXp;
    double  m_dWorkSpeedXpYm;
    double  m_dWorkSpeedXpYp;
    double  m_dWorkSpeedYm;
    double  m_dWorkSpeedYp;
    int     m_iControlType;
    int     m_iPocketTool;
    int     m_iPocketType;
    int     m_iType;
    QString m_strNote;
    bool    m_ToolTest_bActive;
    int     m_iToolType;
    bool    m_bPulseOn;
    double  m_dLsrSpotDistance;


};

#endif // MATERIALTOOL_H
