#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QDeclarativeItem>
#include <QtSql>
#include <QSqlQueryModel>
#include "errorlist.h"
#include "material.h"
#include "mil_material.h"

QT_FORWARD_DECLARE_CLASS(QSqlDatabase)

class Database : public QDeclarativeItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Database)
    Q_PROPERTY(QString dbname READ dbname WRITE setDbName)
    Q_PROPERTY(QString username READ username WRITE setUsername)
    Q_PROPERTY(QString password READ password WRITE setPassword)
    Q_PROPERTY(QString hostname READ hostname WRITE setHostname)
    Q_PROPERTY(int port READ port WRITE setPort)
    Q_PROPERTY(QString result READ result WRITE setResult)
    Q_PROPERTY(QList<QObject*> errorList READ errorList)
    Q_PROPERTY(QList<QObject*> warningList READ warningList)
    Q_PROPERTY(QString language READ language WRITE setLanguage)
    Q_PROPERTY(Material* material READ material NOTIFY materialChanged)
    Q_PROPERTY(QStringList materials READ materials NOTIFY materialsChanged)
    Q_PROPERTY(MilMaterial* milMaterial READ milMaterial NOTIFY milMaterialChanged)
    Q_PROPERTY(QStringList milMaterials READ milMaterials NOTIFY milMaterialsChanged)
    //returns empty string, can be used to initiate the translation: dbTr("sample") + tr
    Q_PROPERTY(QString tr READ tr NOTIFY dbLanguageChanged)
    Q_PROPERTY(QString lasterror READ lasterror NOTIFY lasterrorChanged)

    Q_PROPERTY(QList<QObject*> las_toolsetList READ las_toolsetList)
    Q_PROPERTY(QList<QObject*> mil_toolsetList READ mil_toolsetList)

    Q_PROPERTY(QString strMac_type READ strMac_type WRITE setStrMac_type NOTIFY strMac_typeChanged)

public:
    Database(QDeclarativeItem *parent = 0);
    ~Database();

    QString dbname() const;
    Q_INVOKABLE void setDbName(const QString &dbname);

    QString username() const;
    Q_INVOKABLE void setUsername(const QString &username);

    QString password() const;
    Q_INVOKABLE void setPassword(const QString &password);

    QString hostname() const;
    Q_INVOKABLE void setHostname(const QString &hostname);

    int port() const;
    Q_INVOKABLE void setPort(const int &port);

    QString result() const;
    Q_INVOKABLE void setResult(const QString &result);

    QList<QObject*> errorList();
    QList<QObject*> warningList();

    QSqlQueryModel* errorListModel() const;

    Material* material() const;
    Q_INVOKABLE void fetchMaterial(const QString&);
    Q_INVOKABLE bool fetchMaterial(const int&);

    QStringList materials();

    MilMaterial* milMaterial() const;
    Q_INVOKABLE void fetchMilMaterial(const QString&);
    Q_INVOKABLE bool fetchMilMaterial(const int&);

    QStringList milMaterials();
    void        refreshErrorList();

    QString tr();

    QString lasterror() const;
    Q_INVOKABLE void setLasterror(const QString &strError);

    //void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    QSqlDatabase currentDatabase() const;

    Q_INVOKABLE bool connect();
    Q_INVOKABLE QString dbTr(QString) const;
    Q_INVOKABLE bool execute(QString);

    QString language() const;
    Q_INVOKABLE void setLanguage(const QString &strLanguage);
    Q_INVOKABLE void doTranslate(); //emits dbLanguageChanged signal -- can be called from qml

    QList<QObject*> las_toolsetList();
    QList<QObject*> mil_toolsetList();

    QString strMac_type() const;
    Q_INVOKABLE void setStrMac_type(const QString &str);

    void loadLanguage();

signals:
    void connected();
    void dbLanguageChanged();
    void materialChanged();
    void materialsChanged();
    void milMaterialChanged();
    void milMaterialsChanged();
    void lasterrorChanged();

private:

    QString                     getLang(); // returns 3 chars lang code e.g.: ENG

    QSqlDatabase                m_db;

    QString                     m_driver;
    QString                     m_dbname;
    QString                     m_username;
    QString                     m_password;
    QString                     m_hostname;

    int                         m_port;

    QString                     m_result;
    QList<QObject*>             m_errorList;
    QList<QObject*>             m_warningList;

    QMap<QString, QString>      m_lang;
    QString                     m_strLanguage;
    Material*                   m_Material;
    MilMaterial*                m_milMaterial;
    QString                     m_lasterror;
    //QList<QString>              m_materialList;

    QList<QObject*>             m_las_toolsetList;
    QList<QObject*>             m_mil_toolsetList;
    QString                     m_strMac_type;


};

    QString                     getLastExecutedQuery(const QSqlQuery& query);

#endif // DATABASE_H
