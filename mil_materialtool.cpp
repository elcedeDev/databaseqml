#include "mil_materialtool.h"
#include <QtSql>
#include <QStringBuilder>
#include <QMap>


MilMaterialTool::MilMaterialTool(QObject *parent) :
    QObject(parent)
{
    m_strNote = "Tool Note";
}

int MilMaterialTool::mil_toolset() const
{
    return m_mat_id;
}

void MilMaterialTool::setmil_toolset(const int i)
{
    m_mat_id = i;
}

int MilMaterialTool::num() const
{
 return m_num;
}

void MilMaterialTool::setnum(const int i)
{
    m_num = i;
}

QString MilMaterialTool::strNote() const
{
    return m_strNote;
}

void MilMaterialTool::setstrNote(const QString &str)
{
    m_strNote = str;
}


int MilMaterialTool::iType() const
{
    return m_iType;
}

void MilMaterialTool::setiType(const int i)
{
    m_iType = i;
}



double MilMaterialTool::dDiameter() const
{
    return m_dDiameter;
}

void MilMaterialTool::setdDiameter(const double d)
{
    m_dDiameter = d;
}

double MilMaterialTool::dChannelWidth() const
{
    return m_dChannelWidth;
}

void MilMaterialTool::setdChannelWidth(const double d)
{
    m_dChannelWidth = d;
}


double MilMaterialTool::dSpindleSpeed() const
{
    return m_dSpindleSpeed;
}

void MilMaterialTool::setdSpindleSpeed(const double d)
{
    m_dSpindleSpeed = d;
}


double MilMaterialTool::dSpeedMax() const
{
    return m_dSpeedMax;
}

void MilMaterialTool::setdSpeedMax(const double d)
{
    m_dSpeedMax = d;
}

double MilMaterialTool::dAccelerationMax() const
{
    return m_dAccelerationMax;
}

void MilMaterialTool::setdAccelerationMax(const double d)
{
    m_dAccelerationMax = d;
}


double MilMaterialTool::dWorkHeight() const
{
    return m_dWorkHeight;
}

void MilMaterialTool::setdWorkHeight(const double d)
{
    m_dWorkHeight = d;
}



double MilMaterialTool::dDistanceDepth() const
{
    return m_dDistanceDepth;
}

void MilMaterialTool::setdDistanceDepth(const double d)
{
    m_dDistanceDepth = d;
}


double MilMaterialTool::dPiercingSpeed() const
{
    return m_dPiercingSpeed;
}

void MilMaterialTool::setdPiercingSpeed(const double d)
{
    m_dPiercingSpeed = d;
}


double MilMaterialTool::dToolTest() const
{
    return m_dToolTest;
}

void MilMaterialTool::setdToolTest(const double d)
{
    m_dToolTest = d;
}


double MilMaterialTool::dAngle() const
{
    return m_dAngle;
}

void MilMaterialTool::setdAngle(const double d)
{
    m_dAngle = d;
}



int MilMaterialTool::iPocketTool() const
{
    return m_iPocketTool;
}

void MilMaterialTool::setiPocketTool(const int i)
{
    m_iPocketTool = i;
}


int MilMaterialTool::iPocketType() const
{
    return m_iPocketType;
}

void MilMaterialTool::setiPocketType(const int i)
{
    m_iPocketType = i;
}

bool MilMaterialTool::bSmoothenEdges() const
{
    return m_bSmoothenEdges;
}

void MilMaterialTool::setbSmoothenEdges(const bool b)
{
    m_bSmoothenEdges = b;
}

bool MilMaterialTool::bShortenEdges() const
{
    return m_bShortenEdges;
}

void MilMaterialTool::setbShortenEdges(const bool b)
{
    m_bShortenEdges = b;
}

bool MilMaterialTool::bTurnClockwise() const
{
    return m_bTurnClockwise;
}

void MilMaterialTool::setbTurnClockwise(const bool b)
{
    m_bTurnClockwise = b;
}

bool MilMaterialTool::bLubrication() const
{
    return m_bLubrication;
}

void MilMaterialTool::setbLubrication(const bool b)
{
    m_bLubrication = b;
}

bool MilMaterialTool::bBoarderOnly() const
{
    return m_bBoarderOnly;
}

void MilMaterialTool::setbBoarderOnly(const bool b)
{
    m_bBoarderOnly = b;
}


bool MilMaterialTool::bDelete() const
{
    return m_bDelete;
}

void MilMaterialTool::setbDelete(const bool b)
{
    m_bDelete = b;
}



bool MilMaterialTool::ToolTest_bActive() const
{
    return m_ToolTest_bActive;
}

void MilMaterialTool::setToolTest_bActive(const bool b)
{
    m_ToolTest_bActive = b;
}



bool MilMaterialTool::bZMillCompensation() const
{
    return m_bZMillCompensation;
}

void MilMaterialTool::setbZMillCompensation(const bool b)
{
    m_bZMillCompensation = b;
}


int MilMaterialTool::iEdges() const
{
    return m_iEdges;
}

void MilMaterialTool::setiEdges(const int i)
{
    m_iEdges = i;
}


double MilMaterialTool::dStepover() const
{
    return m_dStepover;
}

void MilMaterialTool::setdStepover(const double d)
{
    m_dStepover = d;
}

int MilMaterialTool::iFunctionType() const
{
    return m_iFunctionType;
}

void MilMaterialTool::setiFunctionType(const int i)
{
    m_iFunctionType = i;
}


bool MilMaterialTool::fetch(QSqlDatabase* db)
{
    QSqlQuery q(*db);
    QString strSQL;
    strSQL = QString("SELECT * FROM mil_material_tool WHERE mat_id = %1 AND num = %2")
                .arg(m_mat_id)
                .arg(m_num);
    q.exec(strSQL);
    if(q.first())
    {
        int i = 0;

        m_mat_id                = q.value(i).toInt(); ++i;
        m_num                   = q.value(i).toInt(); ++i;
        m_strNote               = q.value(i).toString(); ++i;
        m_iType                 = q.value(i).toInt(); ++i;
        m_dDiameter             = q.value(i).toDouble(); ++i;
        m_dChannelWidth         = q.value(i).toDouble(); ++i;
        m_dSpindleSpeed         = q.value(i).toDouble(); ++i;
        m_dSpeedMax             = q.value(i).toDouble(); ++i;
        m_dAccelerationMax      = q.value(i).toDouble(); ++i;
        m_dWorkHeight           = q.value(i).toDouble(); ++i;
        m_dDistanceDepth        = q.value(i).toDouble(); ++i;
        m_dPiercingSpeed        = q.value(i).toDouble(); ++i;
        m_dToolTest             = q.value(i).toDouble(); ++i;
        m_dAngle                = q.value(i).toDouble(); ++i;
        m_iPocketTool           = q.value(i).toInt(); ++i;
        m_iPocketType           = q.value(i).toInt(); ++i;
        m_bSmoothenEdges        = q.value(i).toBool(); ++i;
        m_bShortenEdges         = q.value(i).toBool(); ++i;
        m_bTurnClockwise        = q.value(i).toBool(); ++i;
        m_bLubrication          = q.value(i).toBool(); ++i;
        m_bBoarderOnly          = q.value(i).toBool(); ++i;
        m_bDelete               = q.value(i).toBool(); ++i;
        m_ToolTest_bActive      = q.value(i).toBool(); ++i;
        m_bZMillCompensation    = q.value(i).toBool(); ++i;
        m_iEdges                = q.value(i).toInt(); ++i;
        m_dStepover             = q.value(i).toDouble(); ++i;
        m_iFunctionType         = q.value(i).toInt(); ++i;
        return true;
    }
    else
    {
        qDebug() << "ERROR -- MaterialTool::fetchTool -- tool not found! mat_id:" << m_mat_id << "tool num:" << m_num << strSQL << q.lastError();
        return false;
    }

}

int MilMaterialTool::update(QSqlDatabase* db)
{
    QSqlQuery q(*db);
    QString strSQL = QString("SELECT update_mil_material_tool(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");

    q.prepare(strSQL);
    int i = 0;
    q.bindValue(i, m_mat_id ); ++i;
    q.bindValue(i, m_num ); ++i;
    q.bindValue(i, m_strNote ); ++i;
    q.bindValue(i, m_iType ); ++i;
    q.bindValue(i, m_dDiameter ); ++i;
    q.bindValue(i, m_dChannelWidth ); ++i;
    q.bindValue(i, m_dSpindleSpeed ); ++i;
    q.bindValue(i, m_dSpeedMax ); ++i;
    q.bindValue(i, m_dAccelerationMax ); ++i;
    q.bindValue(i, m_dWorkHeight ); ++i;
    q.bindValue(i, m_dDistanceDepth ); ++i;
    q.bindValue(i, m_dPiercingSpeed ); ++i;
    q.bindValue(i, m_dToolTest ); ++i;
    q.bindValue(i, m_dAngle ); ++i;
    q.bindValue(i, m_iPocketTool ); ++i;
    q.bindValue(i, m_iPocketType ); ++i;
    q.bindValue(i, m_bSmoothenEdges ); ++i;
    q.bindValue(i, m_bShortenEdges ); ++i;
    q.bindValue(i, m_bTurnClockwise ); ++i;
    q.bindValue(i, m_bLubrication ); ++i;
    q.bindValue(i, m_bBoarderOnly ); ++i;
    q.bindValue(i, m_bDelete ); ++i;
    q.bindValue(i, m_ToolTest_bActive ); ++i;
    q.bindValue(i, m_bZMillCompensation ); ++i;
    q.bindValue(i, m_iEdges ); ++i;
    q.bindValue(i, m_dStepover ); ++i;
    q.bindValue(i, m_iFunctionType );
    q.exec();

    //qDebug() << "m_mat_id" << m_mat_id << "m_num:" << m_num << " m_strNote:" << m_strNote ;

    //qDebug() << "fields: " << m_num << "," << m_bBoarderOnly<< ","    << m_bDelete<< ","     << m_bEngravingTool<< ","     << m_bSlotShortEdges<< ","     << m_bSlotSmoothenEdges<< ","     << m_bSlotTurnClockwise<< ","     << m_bZMillCompensation<< ","     << m_dAccelerationMax<< ","     << m_dAccLsrFrequency<< ","     << m_dAccLsrPulseWidth<< ","     << m_dAnalogOutput<< ","     << m_dAngle<< ","     << m_dBeamRotation<< ","     << m_dChannelWidth<< ","     << m_dConstLsrFrequency<< ","     << m_dConstLsrPulseWidth<< ","     << m_dDeccLsrFrequency<< ","     << m_dDeccLsrPulseWidth<< ","     << m_dDiameter<< ","     << m_dDistanceDepth<< ","     << m_dEnvelopeCut<< ","     << m_dHeightXm<< ","     << m_dHeightXmYm<< ","     << m_dHeightXmYp<< ","     << m_dHeightXp<< ","     << m_dHeightXpYm<< ","     << m_dHeightXpYp<< ","     << m_dHeightYm<< ","     << m_dHeightYp<< ","     << m_dLookAheadLimitAngleMin<< ","     << m_dLookAheadLinitAngleMax<< ","     << m_dLsrAnalogPower<< ","     << m_dLsrExponentAnalogPower<< ","     << m_dLsrExponentFrequency<< ","     << m_dLsrFactorAnalogPower<< ","     << m_dLsrFactorFrequency<< ","     << m_dMaxDeviation<< ","     << m_dMaxPulseWidthSpeed<< ","     << m_dPiercingSpeed<< ","     << m_dSpeedMax<< ","     << m_dSpindleSpeed<< ","     << m_dStartMacroAnalogPower<< ","     << m_dStartMacroLsrFrequency<< ","     << m_dStartMacroLsrPulseWidth<< ","     << m_dStartMacroTime<< ","     << m_dStopMacroAnalogPower<< ","     << m_dStopMacroLsrFrequency<< ","     << m_dStopMacroLsrPulseWidth<< ","     << m_dToolTest<< ","     << m_dWorkHeight<< ","     << m_dWorkSpeedXm<< ","     << m_dWorkSpeedXmYm<< ","     << m_dWorkSpeedXmYp<< ","     << m_dWorkSpeedXp<< ","     << m_dWorkSpeedXpYm<< ","     << m_dWorkSpeedXpYp<< ","     << m_dWorkSpeedYm<< ","     << m_dWorkSpeedYp<< ","     << m_iControlType<< ","     << m_iPocketTool<< ","     << m_iPocketType<< ","     << m_iType << ","    << m_strNote << ","    << m_ToolTest_bActive << ","    << m_iToolType  << ","   << m_bPulseOn<< ","     << m_dLsrSpotDistance;
/*
    QMap<QString,QVariant> boundMap = q.boundValues();
    for(auto i = boundMap.begin(); i != boundMap.end(); ++i)
    {
        qDebug() << "boundMap: " << i.key() << i.value();
    }
*/

    if(q.first())
    {
        if( m_num != q.value(0).toInt())
        {
            qDebug() << "1 ERROR running MaterialTool::update() -- num: " << m_num << "  " << q.lastError();
            return -1;
        }
    }
    else
    {
        qDebug() << "2 ERROR running MaterialTool::update() -- num: " << m_num << "  " << q.lastError();
        return -1;
    }
    return m_num;

}
