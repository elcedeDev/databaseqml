TEMPLATE    = lib
TARGET      = Database
QT          += declarative sql
CONFIG      += qt plugin c++11


DESTDIR = components/Database
TARGET = $$qtLibraryTarget($$TARGET)
uri = Database

RC_FILE = proj.rc

# Input
SOURCES += \
    database_plugin.cpp \
    database.cpp \
    errorlist.cpp \
    materialtool.cpp \
    material.cpp \
    mil_materialtool.cpp \
    mil_material.cpp \
    materialtoolset.cpp

HEADERS += \
    database_plugin.h \
    database.h \
    errorlist.h \
    materialtool.h \
    material.h \
    version.h \
    mil_materialtool.h \
    mil_material.h \
    materialtoolset.h

DISTFILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}

OTHER_FILES += \
    main.qml \
    qmltypes/plugins.qmltypes \
    proj.rc
