#include "database_plugin.h"
#include "database.h"

#include <qdeclarative.h>
#include <QtPlugin>


void DatabasePlugin::registerTypes(const char *uri)
{
    // @uri elcede.qmlcomponents
    qmlRegisterType<Database>(uri, 1, 0, "Database");
}

