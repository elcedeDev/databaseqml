#ifndef VERSION_H
#define VERSION_H


#define VER_FILEVERSION             2,0,0
#define VER_FILEVERSION_STR         "2.0.0"

#define VER_PRODUCTVERSION          VER_FILEVERSION
#define VER_PRODUCTVERSION_STR      VER_FILEVERSION_STR

#define VER_COMPANYNAME_STR         "ELCEDE GmbH"
#define VER_FILEDESCRIPTION_STR     "Database handling module for VV8"
#define VER_INTERNALNAME_STR        "Database plugin"
#define VER_LEGALCOPYRIGHT_STR      "Copyright \xa9 2018 ELCEDE GmbH"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "Database.dll"
#define VER_PRODUCTNAME_STR         VER_INTERNALNAME_STR

#define VER_COMPANYDOMAIN_STR       "https://www.elcede.de/"


#define PRINTVERSION    QLocale us = QLocale("en_US"); \
                        QDate date = us.toDate(__DATE__, "MMM dd yyyy"); \
                        QString buildDate =  date.toString(Qt::ISODate); \
                        qDebug() << VER_PRODUCTNAME_STR << "|" << "Version"  <<  VER_FILEVERSION_STR << "|" <<  "Build date" << buildDate.toLatin1().data() << __TIME__;

#endif // VERSION_H

